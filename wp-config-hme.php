<?php

/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'hme');

/** Имя пользователя MySQL */
define('DB_USER', 'hme');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 's1fZtjUpQT5hd3j');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');


/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'TaHoleql[ ^FGb]vw)7#|KM1BLEs%tReAY*2rys[y[nam[@v+i9#]y7X<,Gh<yv*');
define('SECURE_AUTH_KEY',  '!rHLVTR6o#JmhpSELf8E<>}Q`7a/tO.YJxN!{CCQ[,E{Le^K>rG(a4h=/&h_>lK)');
define('LOGGED_IN_KEY',    'oa>D@WUG19AhrgC(jVx._JG1>eL/&0X{dsXSi$My=VnUk~Q@C0DL1N1Z0-/7FaMZ');
define('NONCE_KEY',        '3Aj;0:3<LDS2<b2tnwOQ/)QVbAxu*TYnR+$lGyB-ay@QDljs.ce<>=MD cV=egVx');
define('AUTH_SALT',        'vdGSPG:6c.[}MV,LOGEi>=|_1|ItMLJ*@W!ol^2T`[/xI;F@CjB8LDS.ha@Q9-{>');
define('SECURE_AUTH_SALT', '*=7?Mfmn(v 6a.&jZbItgwiJ!uxzsRyhn_|b#;ve%(}$?v7$xiA_JS@!{~A(QFkJ');
define('LOGGED_IN_SALT',   '(Svd1~{@~6T}we38hj|:<<HL(Ap@).PO3N^<$ A8]IW{E%//5GKu}B3EGJ,dqC*b');
define('NONCE_SALT',       ')i>oR8*K8M,J3(GplIp;*vy&v4BX=2^Ufm2Jnx~_fWT;I%K>h|Hm(:hD@GPA8zc:');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'hm_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
