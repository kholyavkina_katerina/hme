<?php

function theme_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'avada-stylesheet' ) );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function theme_enqueue_footer_styles() {
    wp_register_style( 'swiper_css', get_stylesheet_directory_uri() . '/css/swiper.min.css');
    wp_enqueue_style('swiper_css');
}
add_action( 'get_footer', 'theme_enqueue_footer_styles' );

function avada_lang_setup() {
	$lang = get_stylesheet_directory() . '/languages';
	load_child_theme_textdomain( 'Avada', $lang );
}
add_action( 'after_setup_theme', 'avada_lang_setup' );

wp_register_script('script-js',  get_stylesheet_directory_uri() . '/js/script.js', false, null, true);
wp_enqueue_script('script-js');

wp_register_script('masked', get_stylesheet_directory_uri() . '/js/jquery.maskedinput.js', false, null, true);
wp_enqueue_script('masked');

wp_register_script('swiper', get_stylesheet_directory_uri() . '/js/swiper.min.js', false, null, true);
wp_enqueue_script('swiper');


/* Custom types */

/* Туры   */

add_action( 'init', 'register_tours_post_type' );
function register_tours_post_type() {

	register_taxonomy('tours-cat', array('tours'), array(
		'label'                 => 'Категория', // определяется параметром $labels->name
		'labels'                => array(
			'name'              => 'Категории туров',
			'singular_name'     => 'Категория',
			'search_items'      => 'Искать Категорию',
			'all_items'         => 'Все Категории',
			'parent_item'       => 'Родит. Категория',
			'parent_item_colon' => 'Родит. Категория:',
			'edit_item'         => 'Ред. Категорию',
			'update_item'       => 'Обновить Категорию',
			'add_new_item'      => 'Добавить Категорию',
			'new_item_name'     => 'Новая Категория',
			'menu_name'         => 'Категории',
		),
		'description'           => 'Категории туров', // описание таксономии
		'public'                => true,
		'show_in_nav_menus'     => true, // равен аргументу public
		'show_ui'               => true, // равен аргументу public
		'show_tagcloud'         => false, // равен аргументу show_ui
		'hierarchical'          => true,
		'rewrite'               => array('slug'=>'tours', 'hierarchical'=>false, 'with_front'=>false, 'feed'=>false ),
		'show_admin_column'     => true, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
	) );

	// тип записи - Туры
	register_post_type('tour', array(
		'label'               => 'Туры',
		'labels'              => array(
			'name'          => 'Туры',
			'singular_name' => 'Тур',
			'menu_name'     => 'Туры',
			'all_items'     => 'Все Туры',
			'add_new'       => 'Добавить Тур',
			'add_new_item'  => 'Добавить новуй Тур',
			'edit'          => 'Редактировать',
			'edit_item'     => 'Редактировать Тур',
			'new_item'      => 'Новый Тур',
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'show_ui'             => true,
		'show_in_rest'        => false,
		'rest_base'           => '',
		'show_in_menu'        => true,
		'exclude_from_search' => false,
		'capability_type'     => 'post',
		'map_meta_cap'        => true,
		'hierarchical'        => false,
		'rewrite'             => array( 'slug'=>'tours/%tours-cat%', 'with_front'=>false, 'pages'=>false, 'feeds'=>false, 'feed'=>false ),
		'has_archive'         => 'tours',
		'query_var'           => true,
		'supports'            => array('title','editor','author','excerpt','comments','thumbnail'),
		'taxonomies'          => array( 'tours-cat' ),
	) );

}

add_filter('post_type_link', 'mg_permalink', 1, 2);
function mg_permalink( $permalink, $post ){
	// выходим если это не наш тип записи: без холдера %products%
	if( strpos($permalink, '%tours-cat%') === false )
		return $permalink;

	// Получаем элементы таксы
	$terms = get_the_terms($post, 'tours-cat');
	// если есть элемент заменим холдер
	if( ! is_wp_error($terms) && !empty($terms) && is_object($terms[0]) )
		$term_slug = array_pop($terms)->slug;
	// элемента нет, а должен быть...
	else
		$term_slug = 'no-tours-cat';
	return str_replace('%tours-cat%', $term_slug, $permalink );
}

add_filter( 'template_include', 'tour_template');
function tour_template( $template ) {

        if (get_post_type() == 'tour'){

		if ( is_single() ) {
		if ( $new_template = locate_template( array( '/templates/temp_tour.php' ) ) )
			return $new_template;
		}

}

	return $template;

}

add_action( 'init', 'create_tag_taxonomies', 0 );

//create two taxonomies, genres and tags for the post type "tag"
function create_tag_taxonomies()
{
  // Add new taxonomy, NOT hierarchical (like tags)
  $labels = array(
    'name' => _x( 'Метки', 'taxonomy general name' ),
    'singular_name' => _x( 'Метка', 'taxonomy singular name' ),
    'search_items' =>  __( 'Поиск метки' ),
    'popular_items' => __( 'Популярные метки' ),
    'all_items' => __( 'Все метки' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Изменить метку' ),
    'update_item' => __( 'Добавить метку' ),
    'add_new_item' => __( 'Новая метка' ),
    'new_item_name' => __( 'Новое имя метки' ),
    'separate_items_with_commas' => __( 'Разделять метки запятыми' ),
    'add_or_remove_items' => __( 'Добавить или удалить метку' ),
    'choose_from_most_used' => __( 'Выберите метку' ),
    'menu_name' => __( 'Метки' ),
  );
 register_taxonomy('tour_tags','tour',array(
    'labels' => $labels,
 	'public' => true,
	'show_in_nav_menus' => true,
 	'show_ui' => true,
 	'show_tagcloud' => true,
 	'hierarchical' => false,
 	'show_admin_column' => true,
 	'query_var' => true,
 	'update_count_callback' => '_update_post_term_count',
    'rewrite' => array( 'slug' => 'tour_tag' ),
  ));

}

/* Гиды   */

add_action( 'init', 'register_guides_post_type' );
function register_guides_post_type() {

	register_post_type('guide', array(
		'label'               => 'Гиды',
		'labels'              => array(
			'name'          => 'Гиды',
			'singular_name' => 'Гид',
			'menu_name'     => 'Гиды',
			'all_items'     => 'Все Гиды',
			'add_new'       => 'Добавить Гида',
			'add_new_item'  => 'Добавить нового Гида',
			'edit'          => 'Редактировать',
			'edit_item'     => 'Редактировать Гида',
			'new_item'      => 'Новый Гид',
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'show_ui'             => true,
		'show_in_rest'        => false,
		'rest_base'           => '',
		'show_in_menu'        => true,
		'exclude_from_search' => false,
		'capability_type'     => 'post',
		'map_meta_cap'        => true,
		'hierarchical'        => false,
		'rewrite'             => array( 'slug'=>'guides', 'with_front'=>false, 'pages'=>false, 'feeds'=>false, 'feed'=>false ),
		'has_archive'         => 'guides',
		'query_var'           => true,
		'supports'            => array('title','editor','author','excerpt','comments','thumbnail'),
		'taxonomies'          => array( 'guides-cat' ),
	) );

}

add_filter('post_type_link', 'gd_permalink', 1, 2);
function gd_permalink( $permalink, $post ){
	// выходим если это не наш тип записи: без холдера
	if( strpos($permalink, '%guides-cat%') === false )
		return $permalink;

	// Получаем элементы таксы
	$terms = get_the_terms($post, 'guides-cat');
	// если есть элемент заменим холдер
	if( ! is_wp_error($terms) && !empty($terms) && is_object($terms[0]) )
		$term_slug = array_pop($terms)->slug;
	// элемента нет, а должен быть...
	else
		$term_slug = 'no-guides-cat';
	return str_replace('%guides-cat%', $term_slug, $permalink );
}

add_filter( 'template_include', 'guide_template');
function guide_template( $template ) {

        if (get_post_type() == 'guide'){

		if ( is_single() ) {
		if ( $new_template = locate_template( array( '/templates/temp_onelider.php' ) ) )
			return $new_template;
		} else {
		if ( $new_template = locate_template( array( '/templates/temp_liders.php' ) ) )
			return $new_template;

		}

}

	return $template;

}

add_action( 'init', 'create_tag_taxonomies', 0 );

/* Отзывы   */

add_action( 'init', 'register_testimon_post_type' );
function register_testimon_post_type() {

register_taxonomy('testimonials-cat', array('testimon'), array(
		'label'                 => 'Категория', // определяется параметром $labels->name
		'labels'                => array(
			'name'              => 'Категории',
			'singular_name'     => 'Категория',
			'search_items'      => 'Искать Категорию',
			'all_items'         => 'Все Категории',
			'parent_item'       => 'Родит. Категория',
			'parent_item_colon' => 'Родит. Категория:',
			'edit_item'         => 'Ред. Категорию',
			'update_item'       => 'Обновить Категорию',
			'add_new_item'      => 'Добавить Категорию',
			'new_item_name'     => 'Новая Категория',
			'menu_name'         => 'Категории',
		),
		'description'           => 'Категории', // описание таксономии
		'public'                => true,
		'show_in_nav_menus'     => false, // равен аргументу public
		'show_ui'               => true, // равен аргументу public
		'show_tagcloud'         => false, // равен аргументу show_ui
		'hierarchical'          => true,
		'rewrite'               => array('slug'=>'testimon', 'hierarchical'=>false, 'with_front'=>false, 'feed'=>false ),
		'show_admin_column'     => true, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
	) );

	register_post_type('testimon', array(
		'label'               => 'Отзывы',
		'labels'              => array(
			'name'          => 'Отзывы',
			'singular_name' => 'Отзыв',
			'menu_name'     => 'Отзывы',
			'all_items'     => 'Все Отзывы',
			'add_new'       => 'Добавить Отзыв',
			'add_new_item'  => 'Добавить новый Отзыв',
			'edit'          => 'Редактировать',
			'edit_item'     => 'Редактировать Отзыв',
			'new_item'      => 'Новый Отзыв',
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'show_ui'             => true,
		'show_in_rest'        => false,
		'rest_base'           => '',
		'show_in_menu'        => true,
		'exclude_from_search' => false,
		'capability_type'     => 'post',
		'map_meta_cap'        => true,
		'hierarchical'        => false,
		'rewrite'             => array( 'slug'=>'testimonials', 'with_front'=>false, 'pages'=>false, 'feeds'=>false, 'feed'=>false ),
		'has_archive'         => 'testimonials',
		'query_var'           => true,
		'supports'            => array('title','editor','author','excerpt','comments','thumbnail'),
		'taxonomies'          => array( 'testimonials-cat' ),
	) );

}
add_filter( 'template_include', 'testimon_template');
function testimon_template( $template ) {

        if (get_post_type() == 'testimon'){

		if ( is_single() ) {
		if ( $new_template = locate_template( array( '/templates/temp_onetestimon.php' ) ) )
			return $new_template;
		}
		else {
		if ( $new_template = locate_template( array( '/templates/temp_testimonials.php' ) ) )
			return $new_template;

		}

}

	return $template;

}


function str_todate($arg){

$yea = substr($arg, 0, 4);
$mon = substr($arg, 4, 2);
$day = substr($arg, 6, 2);
$tur_date = $day.'.'.$mon.'.'.$yea;
return $tur_date;
}

function str_month($arg){

switch ($arg) {
    case 1:
        $str_month = "Январь";
        break;
    case 2:
         $str_month = "Февраль";
        break;
    case 3:
         $str_month = "Март";
        break;
    case 4:
         $str_month = "Апрель";
        break;
    case 5:
         $str_month = "Май";
        break;
    case 6:
         $str_month = "Июнь";
        break;
    case 7:
         $str_month = "Июль";
        break;
    case 8:
         $str_month = "Август";
        break;
    case 9:
         $str_month = "Сентябрь";
        break;
    case 10:
         $str_month = "Октябрь";
        break;
    case 11:
         $str_month = "Ноябрь";
        break;
    case 12:
         $str_month = "Декабрь";
        break;
}

return $str_month;
}


function load_tours(){
    $cat_id= $_POST['cat_id'];
	$args = array('post_type' => 'tour',
    'posts_per_page' => -1,
    'tax_query' => array(
		array(
			'taxonomy' => 'tours-cat',
			'field'    => 'id',
			'terms'    => array( $cat_id )
		)
	),
    'meta_key' => 'date_start',
    'orderby' => 'meta_value_num',
    'order' => 'ASC',
    'meta_query' => array(
        array(
            'key' => 'date_start',
            'value' => $today,
            'compare' => '>',
        ),
    ),
);

$my_query = new WP_Query( $args );
$cat_name = get_term($cat_id, 'tours-cat');
echo '<p class="tour_country">' . $cat_name->name . '<p>';
if ( $my_query->have_posts() ) {
	$i=1;
	$calendar_month_year=0;

    while ( $my_query->have_posts() ) {
        $my_query->the_post();
if ($calendar_month_year==0)  {
	$calendar_month_year=substr(get_field( 'date_start' ), 0, 6);
	$yea = substr($calendar_month_year, 0, 4);
    $mon = substr($calendar_month_year, 4, 2);
    echo '<div class="row list_tours">';
	echo '<div class="col-md-2"><p class="calend_month">' . str_month($mon) . '</br>'. $yea .'</p></div>';
	echo '<div class="col-md-10">';
}
if ($calendar_month_year!=substr(get_field( 'date_start' ), 0, 6)){
	$calendar_month_year=substr(get_field( 'date_start' ), 0, 6);
	$yea = substr($calendar_month_year, 0, 4);
    $mon = substr($calendar_month_year, 4, 2);
	echo '</div></div><div class="row list_tours">';
	echo '<div class="col-md-2"><p class="calend_month">' . str_month($mon) . '</br>'. $yea .'</p></div>';
	echo '<div class="col-md-10">';
}

echo '<div class="col-md-4"><a href="'. get_permalink() .' "><div class="block" style="background-image: url('. get_the_post_thumbnail_url( $id, 'medium') . ');">';
echo '<div class="tur_info"><h3 class="tur_name tur_calendar">' . $my_query->posts[$i-1]->post_title .'</h3>';
if (get_field( 'date_start' )) {
echo '<p class="tur_date">'. str_todate(get_field( 'date_start' ));
if (get_field( 'date_finish' ))
echo ' - '. str_todate(get_field( 'date_finish' )) . '</p></div></div></a></div>';
}
$i=$i+1;

    }
echo '</div></div>';
	} else {

     echo '<span class="no_found">В выбранную Вами страну в ближайшее время экспедиций нет.</span>';
	}

	wp_reset_postdata();
	die();
}
add_action('wp_ajax_myaction', 'load_tours');
add_action('wp_ajax_nopriv_myaction', 'load_tours');



add_filter('excerpt_more', function($more) {
	return '...';
});

add_shortcode( 'day_num', 'day_num_func' );
function day_num_func( $atts, $content ) {
	 return '<p class="day_num">'.$content.'</p>';
}

add_shortcode( 'day_title', 'day_title_func' );
function day_title_func( $atts, $content ) {
	 return '<p class="day_title">'.$content.'</p>';
}

add_shortcode( 'tour_price', 'tour_price_func' );
function tour_price_func( $atts, $content ) {
	 return '<p class="tour_price">'.$content.'</p>';
}

add_shortcode( 'tour_data', 'tour_data_func' );
function tour_data_func( $atts, $content ) {
	 return '<p class="tour_data">'.$content.'</p>';
}

add_shortcode( 'tour_length', 'tour_length_func' );
function tour_length_func( $atts, $content ) {
	 return '<p class="tour_length">'.$content.'</p>';
}

add_shortcode( 'col_group', 'col_group_func' );
function col_group_func( $atts, $content ) {
	 return '<p class="col_group">'.$content.'</p>';
}

add_shortcode( 'tour_people_total', 'tour_people_total_func' );
function tour_people_total_func( $atts, $content ) {
	 return '<p class="tour_people_total">'.$content.'</p>';
}
add_shortcode( 'tour_extra_price', 'tour_extra_price_func' );
function tour_extra_price_func( $atts, $content ) {
	 return '<p class="tour_extra_price">'.$content.'</p>';
}

add_shortcode( 'p', 'p_func' );
function p_func( $atts, $content ) {
	 return '<p>'.$content.'</p>';
}

add_shortcode( 'tour_info', 'tour_info_func' );
function tour_info_func( $atts, $content ) {

    $a = '';
    $a .= '<div class="tour_prices_wrapper">';
    $a .= get_field( 'tour_price' ) ?         do_shortcode('[tour_price]' . get_field( 'tour_price' ) . '[/tour_price]')  : '' ;
    $a .= get_field( 'tour_price_1' ) ?       do_shortcode('[tour_extra_price]' . get_field( 'tour_price_1' ) . '[/tour_extra_price]')  : '' ;
    $a .= get_field( 'tour_price_2' ) ?       do_shortcode('[tour_extra_price]' . get_field( 'tour_price_2' ) . '[/tour_extra_price]')  : '' ;
    $a .= get_field( 'tour_price_3' ) ?       do_shortcode('[tour_extra_price]' . get_field( 'tour_price_3' ) . '[/tour_extra_price]')  : '' ;
    $a .= '</div>';
    $a .= get_field( 'tour_dates' ) ?         do_shortcode('[tour_data]' . get_field( 'tour_dates' ) . '[/tour_data]')  : '' ;
    $a .= get_field( 'tour_length' ) ?        do_shortcode('[tour_length]' . get_field( 'tour_length' ) . '[/tour_length]')  : '' ;
    $a .= get_field( 'tour_people_total' ) ?  do_shortcode('[tour_people_total]' . get_field( 'tour_people_total' ) . '[/tour_people_total]')  : '' ;
    $a .= get_field( 'tour_people_left' ) ?   do_shortcode('[p]' . get_field( 'tour_people_left' ) . '[/p]')  : '' ;

    $similar_tours = get_field( 'tour_repeats' ) ? get_field( 'tour_repeats' ) : null;
    if ($similar_tours !== null) :
      $a .= '<div class="similar-tours-calendar">';
      $a .= '<div><strong>Не подходят даты?</strong><br />Экспедиция состоится также:</div>';
      foreach ($similar_tours as $similar_tour):
        $tour_starts = get_field( 'date_start' , $similar_tour->ID ) ? date_parse(get_field( 'date_start' , $similar_tour->ID )) : '';
        $tour_link = get_post_permalink($similar_tour->ID);

        $a .= '<div class="similar-tours-item" '.
              'data-start-month="' . $tour_starts[month] . '"' .
              'data-start-year="' . $tour_starts[year] . '"' .
              'data-href="' . $tour_link . '"' .
              '></div>';
      endforeach;
      $a .= '</div>';
    endif;


    return $a;
}


add_shortcode( 'youtube_carousel', 'youtube_carousel_func' );
function youtube_carousel_func( $atts ) {
  $a  = '';
  $atts = shortcode_atts( array(
  		'ids' => '',
  	), $atts, 'ids' );

  if(esc_html($atts['ids'])):
    $a .= '<div id="youtube_carousel" class="youtube_carousel">';
    $a .= '<div class="swiper-container">';
    $a .= '<div class="swiper-wrapper">';

    // разбиваем строку по произвольному числу запятых и пробельных символов,
    // которые включают в себя  " ", \r, \t, \n и \f
    $video_ids = preg_split("/[\s,]+/", esc_html($atts['ids']));

    foreach ( $video_ids as $video_id ):
          $a .= '<div class="swiper-slide"><div class="youtube_wrapper">';
          $a .= '<iframe width="560" height="315" src="https://www.youtube.com/embed/' . $video_id . '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
          $a .= '</div></div>';
    endforeach;

    $a .= '</div>';
    $a .= '<div class="swiper-pagination"></div>';
    $a .= '<div class="swiper-button-prev"></div>';
    $a .= '<div class="swiper-button-next"></div>';
    $a .= '</div>';
    $a .= '</div>';

  endif;
  return $a;
}
