<?php
/**
 * Archives template.
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?>
<?php $class = get_queried_object();

if ($class->name=='tour'){ 
echo '<div class="more_tours archiv"><div class="grid_block container">';
$args = array('post_type' => 'tour',
   			 'posts_per_page' => -1,
   			 'meta_key' => 'date_start',
   			 'orderby' => 'meta_value_num',
    		 'order' => 'ASC',
   			 );

$my_query = new WP_Query( $args );


if ( $my_query->have_posts() ) {
    $i=1;
    while ( $my_query->have_posts() ) {
        $my_query->the_post();
        echo '<div class="col-md-3"><a href="'. get_permalink() .' "><div class="block" style="background-image: url('. get_the_post_thumbnail_url( $id, 'medium') . ');">';
        echo '<div class="tur_info"><h3 class="tur_name">' . $my_query->posts[$i-1]->post_title .'</h3>';
        if (get_field( 'date_start' )) {
		echo '<p class="tur_date">'. str_todate(get_field( 'date_start' ));
		if (get_field( 'date_finish' ))
		echo ' - '. str_todate(get_field( 'date_finish' )) ;
	    echo '</p>';	
		}
		echo '</div></div></div></a>';
		$i=$i+1;
}

}
echo '</div></div>';
wp_reset_postdata();


} else {
if ($class->taxonomy=='tours-cat'){
$today=date('Ymd');	
$cat_id = $class->term_taxonomy_id;
echo '<div class="more_tours archiv"><div class="grid_block container">';
$my_query = new WP_Query( array('posts_per_page' => -1,
	'tax_query' => array(
		array(
			'taxonomy' => 'tours-cat',
			'field'    => 'id',
			'terms'    => $cat_id
		)
	), 'meta_key' => 'date_start',
   			 'orderby' => 'meta_value_num',
    		 'order' => 'ASC',
			 'meta_query' => array(
        array(
            'key' => 'date_start',
            'value' => $today,
            'compare' => '>',
        ),
    ),
) );


if ( $my_query->have_posts() ) {
  
$i=1; 
	$calendar_month_year=0;
	
    while ( $my_query->have_posts() ) {
        $my_query->the_post();
if ($calendar_month_year==0)  {
	$calendar_month_year=substr(get_field( 'date_start' ), 0, 6);
	$yea = substr($calendar_month_year, 0, 4); 
    $mon = substr($calendar_month_year, 4, 2);
    echo '<div class="row country_tours">';
	echo '<div class="col-md-2"><p class="calend_month">' . str_month($mon) . '</br>'. $yea .'</p></div>';
	echo '<div class="col-md-10">';
}
if ($calendar_month_year!=substr(get_field( 'date_start' ), 0, 6)){
	$calendar_month_year=substr(get_field( 'date_start' ), 0, 6);
	$yea = substr($calendar_month_year, 0, 4); 
    $mon = substr($calendar_month_year, 4, 2);
	echo '</div></div><div class="row country_tours">';
	echo '<div class="col-md-2"><p class="calend_month">' . str_month($mon) . '</br>'. $yea .'</p></div>';
	echo '<div class="col-md-10">';
}

echo '<div class="col-md-4"><a href="'. get_permalink() .' "><div class="block" style="background-image: url('. get_the_post_thumbnail_url( $id, 'medium') . ');">';
echo '<div class="tur_info"><h3 class="tur_name tur_calendar">' . $my_query->posts[$i-1]->post_title .'</h3>';
if (get_field( 'date_start' )) {
echo '<p class="tur_date">'. str_todate(get_field( 'date_start' ));
if (get_field( 'date_finish' ))
echo ' - '. str_todate(get_field( 'date_finish' )) . '</p></div></div></a></div>';	
}
$i=$i+1;

    }
echo '</div></div>';   

} else {
	dynamic_sidebar( 'No_tour' );
}
wp_reset_postdata();
?>
</div>
</main>
<div class="row list_another_countrys" >
<div class="container">
<h3>Экспедиции в другие страны</h3>
<div class="col-md-2"><?php   dynamic_sidebar( 'Country_left_menu' ); ?></div>
<div class="col-md-10">
<?php   dynamic_sidebar( 'Country_right_menu' );
/* $col_cat = wp_count_terms('tours-cat');
 $args = array('taxonomy'=>'tours-cat','orderby'=>'title','show_count'=>0,'pad_counts'=>0,'hierarchical'=>1,'hide_empty'=>0);
          $catlist = get_categories($args);
          $i=1; $total=1;
          echo '<div class="row country">';
           foreach ($catlist as $categories_item) {
           	
           	if ($i==1){
           	echo '<div class="col-md-2"><ul>';	
           	}
             echo '<li><a href="'.site_url().'/tours/'.$categories_item->slug.'">'.$categories_item->cat_name . '</a></li>';
            
             if (($i==5) || ($total==$col_cat)){
             echo '</ul></div>';
             }
             $i=$i+1;
             $total=$total+1;
             if ($i==6)
             	$i=1;

           	}*/
        ?>      
</div>
</div>
</div>
</div>
<div class="container country_description">
<?php 
$class = get_queried_object();
echo $class->description;
?>
</div>
<?php } else {

?>

<section id="content" <?php Avada()->layout->add_class( 'content_class' ); ?> <?php Avada()->layout->add_style( 'content_style' ); ?>>
	<?php if ( category_description() ) : ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class( 'fusion-archive-description' ); ?>>
			<div class="post-content">
				<?php echo category_description(); ?>
			</div>
		</div>
	<?php endif; ?>

	<?php get_template_part( 'templates/blog', 'layout' ); ?>
</section>
<?php 
do_action( 'avada_after_content' );
} } ?>

<?php get_footer();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
