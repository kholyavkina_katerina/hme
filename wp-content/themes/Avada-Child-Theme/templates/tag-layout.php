<?php
/**
 * Blog-layout template.
 *
 * @author     ThemeFusion
 * @copyright  (c) Copyright by ThemeFusion
 * @link       http://theme-fusion.com
 * @package    Avada
 * @subpackage Core
 * @since      1.0.0
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) { exit( 'Direct script access denied.' ); }

global $wp_query;

?>
<div id="posts-container" class="fusion-blog-archive <?php echo esc_attr( $wrapper_class ); ?>fusion-clearfix">
	<div class="<?php echo esc_attr( $container_class ); ?>" data-pages="<?php echo (int) $number_of_pages; ?>">
		
		<?php while ( have_posts() ) : the_post(); ?>
			<?php
			// Set the time stamps for timeline month/year check.
			$alignment_class = '';
			
			// Set the has-post-thumbnail if a video is used. This is needed if no featured image is present.
			$thumb_class = '';
			

			$post_classes = array(
				$post_class,
				$alignment_class,
				$thumb_class,
				$element_orientation_class,
				'post',
				'fusion-clearfix',
			);
			?>
			
			<article id="post-<?php the_ID(); ?>" class="col-md-6 tour_tags">
				<a href="<?php echo get_permalink( $post->ID ) ?>">
				<div class="block_tag" style="background-image: url('<?php echo get_the_post_thumbnail_url( $post->ID, 'medium')?>  ');">
				<div class="fusion-post-content post-content tur_info">
				<h2><?php the_title( );?></h2>
				<?php if (get_field( 'date_start' )) {
               echo '<p class="tur_date">'. str_todate(get_field( 'date_start' ));
              if (get_field( 'date_finish' ))
              echo ' - '. str_todate(get_field( 'date_finish' )) . '</p>'; 
      			} ?>	
				</div> 
				</div>
		        </a>
			</article>

			

		<?php endwhile; ?>

		
	</div>

<?php fusion_pagination( '', 2 ); ?>
</div>
<?php

wp_reset_postdata();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
