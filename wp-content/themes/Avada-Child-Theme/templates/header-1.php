<?php
/**
 * Header-1 template.
 *
 * @author     ThemeFusion
 * @copyright  (c) Copyright by ThemeFusion
 * @link       http://theme-fusion.com
 * @package    Avada
 * @subpackage Core
 */

// Do not allow directly accessing this file.
?>
<style>
.fusion-logo {
    z-index: 100;
}
.top_sidebar {
    position: absolute;
    z-index: 1;
    padding-left: 180px;
	padding-top: 7px;
 }
 @media only screen and (max-width:1060px) {

.top_sidebar {                                   
    font-size: 14px;
	padding-top:10px;
}	
 }
@media only screen and (max-width:800px) {
.top_sidebar {                              
    font-size: 16px;
	padding-top:0px;
	margin-top: -19px;
    line-height: 20px;
}
}
@media only screen and (max-width:480px) {
	
.top_sidebar {                                    
    font-size: 12px;
	padding-left: 160px;
}	
}
@media only screen and (max-width:380px) {
	.top_sidebar {                                    
    font-size: 10px;
	padding-left: 110px;
}	
}
</style>
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<div class="fusion-header-sticky-height"></div>
<div class="fusion-header">
	<div class="fusion-row">
		<?php avada_logo(); ?>
		<div class="top_sidebar">
		<?php dynamic_sidebar("Top_sidebar");  ?>
		</div>
		<?php avada_main_menu(); ?>
	</div>
</div>
