<?php
/**
 *
 * Template Name: Calendar
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?>

<div class="row list_countrys">
<div class="col-md-2"><?php dynamic_sidebar( 'Country_left_menu' ); ?></div>
<div class="col-md-10"><?php dynamic_sidebar( 'Country_right_menu' ); ?>
<?php
 /*$col_cat = wp_count_terms('tours-cat');
 $today=date('Ymd');
 $args = array('taxonomy'=>'tours-cat','orderby'=>'title','show_count'=>0,'pad_counts'=>0,'hierarchical'=>1,'hide_empty'=>0);
          $catlist = get_categories($args);
          $i=1; $total=1;
          echo '<div class="row country">';
           foreach ($catlist as $categories_item) {

           	if ($i==1){
           	echo '<div class="col-md-2"><ul>';
           	}
             echo '<li><a href="'.site_url().'/tours/'.$categories_item->slug.'">'.$categories_item->cat_name . '</a></li>';

             if (($i==5) || ($total==$col_cat)){
             echo '</ul></div>';
             }
             $i=$i+1;
             $total=$total+1;
             if ($i==6)
             	$i=1;

           	}*/
        ?>
</div>
</div>
</div>
<div class="calendar container">

	 <div class="fusion-button-wrapper fusion-aligncenter calendar_buttons_wrapper">
		<a data-category="tour_item" class="calendar_filter fusion-button button-flat fusion-button-round button-large button-default button-1" target="_self" href="#">
			<span class="fusion-button-text">Все туры</span>
		</a>
		<a data-category="category_ekspedicia" class="calendar_filter fusion-button button-flat fusion-button-round button-large button-default button-1" target="_self" href="#">
			<span class="fusion-button-text">Экспедиции</span>
		</a>
		<a data-category="category_ekstrim-tur" class="calendar_filter fusion-button button-flat fusion-button-round button-large button-default button-1" target="_self" href="#">
			<span class="fusion-button-text">Экстрим</span>
		</a>
		<a data-category="category_tury-s-detmi" class="calendar_filter fusion-button button-flat fusion-button-round button-large button-default button-1" target="_self" href="#">
			<span class="fusion-button-text">Туры с детьми</span>
		</a>
	</div>

<?php
$args = array('post_type' => 'tour',
    'posts_per_page' => -1,
    'meta_key' => 'date_start',
    'orderby' => 'meta_value_num',
    'order' => 'ASC',
    'meta_query' => array(
        array(
            'key' => 'date_start',
            'value' => $today,
            'compare' => '>',
        ),
    ),
);

$my_query = new WP_Query( $args );

if ( $my_query->have_posts() ) {
	$i=1;
	$calendar_month_year=0;

    while ( $my_query->have_posts() ) {
        $my_query->the_post();
if ($calendar_month_year==0)  {
	$calendar_month_year=substr(get_field( 'date_start' ), 0, 6);
	$yea = substr($calendar_month_year, 0, 4);
    $mon = substr($calendar_month_year, 4, 2);
    echo '<div class="row list_tours">';
	echo '<div class="col-md-2"><p class="calend_month">' . str_month($mon) . '</br>'. $yea .'</p></div>';
	echo '<div class="col-md-10">';
}
if ($calendar_month_year!=substr(get_field( 'date_start' ), 0, 6)){
	$calendar_month_year=substr(get_field( 'date_start' ), 0, 6);
	$yea = substr($calendar_month_year, 0, 4);
    $mon = substr($calendar_month_year, 4, 2);
	echo '</div></div><div class="row list_tours">';
	echo '<div class="col-md-2"><p class="calend_month">' . str_month($mon) . '</br>'. $yea .'</p></div>';
	echo '<div class="col-md-10">';
}



$categories = get_the_terms( $id, 'tours-cat' );
$class_name = '';
foreach ($categories as $category) {
	$class_name .= ' category_' . $category->slug;
}

echo '<div class="col-md-4 tour_item'. $class_name .'"><a href="'. get_permalink() .' "><div class="block" style="background-image: url('. get_the_post_thumbnail_url( $id, 'medium') . ');">';
echo '<div class="tur_info"><h3 class="tur_name tur_calendar">' . $my_query->posts[$i-1]->post_title .'</h3>';
if (get_field( 'date_start' )) {
echo '<p class="tur_date">'. str_todate(get_field( 'date_start' ));
if (get_field( 'date_finish' ))
echo ' - '. str_todate(get_field( 'date_finish' )) . '</p></div></div></a></div>';
}
$i=$i+1;

    }
echo '</div></div>';
	}


?>

</div>
<section id="content" <?php Avada()->layout->add_style( 'content_style' ); ?>>
	<?php while ( have_posts() ) : the_post(); ?>



			<div class="post-content">
				<?php the_content(); ?>

			</div>

		</div>
	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
</section>
<?php do_action( 'avada_after_content' ); ?>
<?php get_footer();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
