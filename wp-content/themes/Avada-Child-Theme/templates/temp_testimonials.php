<?php
/**
 * 
 * Testimonials
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?>

</div>

<div class="container testimon">

<?php
$args = array('post_type' => 'testimon',
    'posts_per_page' => -1,
        
);

$my_query = new WP_Query( $args );

if ( $my_query->have_posts() ) {
	
	  $i=1;
    while ( $my_query->have_posts() ) {
      $my_query->the_post();
?>
<div class="row testimon_row">
<?php	  
     if($i==1){
      echo '<div class="col-md-4 testimonials">';
      echo '<div class="testimon_img">'.get_the_post_thumbnail( $post->ID, 'medium').'</div>';
      echo '</div><div class="col-md-8 testimonials descr">';
      
      if (get_field( 'tour_name' )) 
      echo '<div class="testimon_tour_name">' . get_field( 'tour_name' ) . '</div>';
      the_excerpt(); ?>
	   <a href="<?php echo get_permalink(); ?>"> Читать далее ...</a>
     <?php
      echo '<div class="testimon_name">';
      the_title( );
      echo '</div>';
	  if (get_field( 'facebook' ))
      echo '<div class="facebook">' . get_field( 'facebook' ) . '</div>';
      if (get_field( 'screen' )){
      echo do_shortcode('[fusion_modal name="scr'.$post->ID.'" title="" size="large" background="" border_color="" show_footer="no" class="" id=""]<img src="'.get_field( 'screen' ).'">[/fusion_modal]');
      echo '<a href="#" data-toggle="modal" data-target=".fusion-modal.scr'.$post->ID.'">Оригинал отзыва</a>';
    }
	 echo '</div>';
     } else {

      echo '<div class="col-md-8 testimonials descr">';
      if (get_field( 'tour_name' )) 
      echo '<div class="testimon_tour_name">' . get_field( 'tour_name' ) . '</div>';
       the_excerpt(); ?>
	   <a href="<?php echo get_permalink(); ?>"> Читать далее ...</a>
     <?php 
      echo '<div class="testimon_name">';
      the_title( );
      echo '</div>';
	  if (get_field( 'facebook' ))
      echo '<div class="facebook">' . get_field( 'facebook' ) . '</div>';
      if (get_field( 'screen' )){
      echo do_shortcode('[fusion_modal name="scr'.$post->ID.'" title="" size="large" background="" border_color="" show_footer="no" class="" id=""]<img src="'.get_field( 'screen' ).'">[/fusion_modal]');
      echo '<a href="#" data-toggle="modal" data-target=".fusion-modal.scr'.$post->ID.'">Оригинал отзыва</a>';
    }
      echo '</div><div class="col-md-4 testimonials">';
      echo '<div class="testimon_img">'.get_the_post_thumbnail( $post->ID, 'medium').'</div></div>';


     }
   
	 $i=$i+1;
     if ($i==3)
     $i=1; 
    ?>
</div>
<?php  } }?>

<?php wp_reset_postdata(); ?>
<?php get_footer(); ?>