<?php
/**
 * 
 * Liders
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?>

</div>

<div class="container">
<div class="testimon">

<?php
$args = array('post_type' => 'guide',
    'posts_per_page' => -1,
    'orderby' => 'title',
    'order' => 'ASC',    
);

$my_query = new WP_Query( $args );

if ( $my_query->have_posts() ) {
	
	 
    while ( $my_query->have_posts() ) {
      $my_query->the_post();
    
      echo '<div class="guide"> <div class="col-md-4">'; ?>
	  <a href="<?php echo the_permalink() ?>">
	  <?php
      echo '<div class="guide_img">'.get_the_post_thumbnail( $post->ID, 'medium').'</div></a>';
      echo '</div><div class="col-md-8">'; ?>
      <a class="lider_name" href="<?php echo the_permalink() ?>"><?php echo the_title()?></a>
      <?php
      the_content();
      /*if (get_field( 'additional' ))
      echo get_field( 'additional' );*/
    $link=get_the_permalink();
      echo do_shortcode('[fusion_button link="'.$link.'" title="" target="_self" link_attributes="" alignment="" modal="" hide_on_mobile="small-visibility,medium-visibility,large-visibility" class="" id="" color="default" button_gradient_top_color="" button_gradient_bottom_color="" button_gradient_top_color_hover="" button_gradient_bottom_color_hover="" accent_color="" accent_hover_color="" type="" bevel_color="" border_width="" size="" stretch="default" shape="" icon="" icon_position="left" icon_divider="no" animation_type="" animation_direction="left" animation_speed="0.3" animation_offset=""]Подробнее[/fusion_button]');    
      echo '</div>';
        
       
     echo '</div>';
    
      
    }
	}


?>

</div></div>

<?php wp_reset_postdata(); ?>
<?php get_footer(); ?>