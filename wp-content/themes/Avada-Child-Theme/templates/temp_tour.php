<?php
/**
 * Template used for single posts and other post-types
 * that don't have a specific template.
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?>

<section id="content" style="width:100%;" <?php Avada()->layout->add_style( 'content_style' ); ?>>



	<?php while ( have_posts() ) : the_post(); ?>
    <?php if ( has_post_thumbnail() ) { ?>
    <div class="block single_tour" style="background-image: url(' <?php echo get_the_post_thumbnail_url( $id, 'full')?> ');">
    <div class="tur_info">
    <h1 class="tur_name"><?php the_title()?></h1>
    <?php if (get_field( 'date_start' )) { ?>
    <p class="tur_date"><?php echo str_todate(get_field( 'date_start' ));
	if (get_field( 'date_finish' ))
	echo ' - ' . str_todate(get_field( 'date_finish' )); ?>
	</p>
    <?php }	}?>
    </div>


		<?php
			// tour tour_icons
			$level = get_field_object( 'tour_level' ) ? get_field_object( 'tour_level' ) : null ;
			$features = get_field_object( 'tour_features' ) ? get_field_object( 'tour_features' ) : null ;

			// var_dump($level);
			// var_dump($features);

			if($level['value'] || $features['value']): ?>
						<div id="tour_icons" class="tour_icons">
							<!-- Slider main container -->
							<div class="swiper-container">
						    <!-- Additional required wrapper -->
						    <div class="swiper-wrapper">


										<?php
										// tour level icon
										if($level['value']):
												$level_slug = $level['value'];
												$level_name = $level['choices'][$level_slug];
												echo '<div class="swiper-slide"><div class="tour_icon">';
													echo '<img class="tour_icon_img ' . $level_slug . '" src="' . get_stylesheet_directory_uri(). '/images/icons/' . $level_slug . '.png" />';
													echo '<div class="tour_icon_title">' . $level_name . '</div>';
												echo '</div></div>';
										endif;

										// tour features  icons
										if($features['value']):
												foreach ( $features['value'] as $feature_slug ):
													$feature_name = $features['choices'][$feature_slug];
													echo '<div class="swiper-slide"><div class="tour_icon">';
														echo '<img class="tour_icon_img ' . $feature_slug . '" src="' . get_stylesheet_directory_uri(). '/images/icons/' . $feature_slug . '.png" />';
														echo '<div class="tour_icon_title">' . $feature_name . '</div>';
													echo '</div></div>';
												endforeach;
										endif;
										?>

									</div>

							    <!-- If we need pagination -->
							    <div class="swiper-pagination"></div>

							    <!-- If we need navigation buttons -->
							    <div class="swiper-button-prev"></div>
							    <div class="swiper-button-next"></div>
							</div>
						</div>
		<?php
			endif;
		?>

<!-- <div class="tour_icons">

		<div class="swiper-container">

		    <div class="swiper-wrapper">

		        <div class="swiper-slide">Slide 1</div>
		        <div class="swiper-slide">Slide 2</div>
		        <div class="swiper-slide">Slide 3</div>
		    </div>

		    <div class="swiper-pagination"></div>


		    <div class="swiper-button-prev"></div>
		    <div class="swiper-button-next"></div>
		</div>
</div> -->

<style>
	.swiper-container {
	    width: 100%;
	    height: 100%;
	}
</style>


	</div>


	<?php if (get_field( 'tour_menu' ))
			echo get_field( 'tour_menu' ); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class( 'post' ); ?>>
			<div class="post-content">
				<?php the_content(); ?>
			</div>
        <div class="more_tours">

        	<?php
			$today=date('Ymd');

			echo '<div class="grid_block container">';
			echo '<h3 class="title-heading-left">Ближайшие туры:</h3>';
			$args = array('post_type' => 'tour',
   			 'posts_per_page' => 4,
   			 'meta_key' => 'date_start',
   			 'orderby' => 'meta_value_num',
    		 'order' => 'ASC',
   			 'meta_query' => array(
   			  array(
            'key' => 'date_start',
            'value' => $today,
            'compare' => '>',
        ),
    ),
);

$my_query = new WP_Query( $args );

if ( $my_query->have_posts() ) {
    $i=1;
    while ( $my_query->have_posts() ) {
        $my_query->the_post();
        echo '<div class="col-md-3"><a href="'. get_permalink() .' "><div class="block" style="background-image: url('. get_the_post_thumbnail_url( $id, 'medium') . ');">';
        echo '<div class="tur_info"><h3 class="tur_name">' . $my_query->posts[$i-1]->post_title .'</h3>';
        if (get_field( 'date_start' )) {
		echo '<p class="tur_date">'. str_todate(get_field( 'date_start' ));
		if (get_field( 'date_finish' ))
		echo ' - '. str_todate(get_field( 'date_finish' )) ;
	    echo '</p>';
		}
		echo '</div></div></div></a>';
		$i=$i+1;

}

}
wp_reset_postdata();
?>
        </div>
		<div id="a8" class="sharing">
		<div class="container">
		<?php dynamic_sidebar( 'Bottom_tour' ); ?>
		<?php avada_render_social_sharing(); ?>
		</div></div>

		</article>

	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
</section>

<?php //do_action( 'avada_after_content' ); ?>
<?php get_footer();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
