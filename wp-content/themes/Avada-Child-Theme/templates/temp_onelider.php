<?php
/**
 * Template used for single posts and other post-types
 * that don't have a specific template.
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
  exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?>

<section id="content" style="width:100%;" <?php Avada()->layout->add_style( 'content_style' ); ?>>


  <?php while ( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" class="testimon">
  
     <?php
     echo '<div class="col-md-4 one_guide">';
      echo '<div class="one_guide_img">'.get_the_post_thumbnail( $post->ID, 'medium').'</div>';
      echo '</div><div class="col-md-8 additional_descr">';
      
      the_content();
      if (get_field( 'additional' )) 
      echo '<div class="additional_tour_name">' . get_field( 'additional' ) . '</div>';
      
          
      echo '</div>';
      
     
      ?>

    </article>
  <?php endwhile; ?>
  <?php $lider_id=get_the_ID(); ?>
  <?php wp_reset_postdata(); ?>
 
  <div class="more_tours lider">
  <h3 class="title-heading-left" style="padding-left:15px;">Туры с этим лидером:</h3>  
  <?php $args = array('post_type' => 'tour',
         'posts_per_page' => -1,
         'meta_key' => 'date_start',
         'orderby' => 'meta_value_num',
         'order' => 'ASC',
      
);

$my_query = new WP_Query( $args );

if ( $my_query->have_posts() ) {
    $i=1;
    while ( $my_query->have_posts() ) {
       $my_query->the_post();
       if (get_field('tour_lider')[0]->ID== $lider_id) {
       
        echo '<div class="col-md-4"><a href="'. get_permalink() .' "><div class="block" style="background-image: url('. get_the_post_thumbnail_url( $id, 'medium') . ');">';
        echo '<div class="tur_info"><h3 class="tur_name">' . get_the_title() .'</h3>';
        if (get_field( 'date_start' )) {
    echo '<p class="tur_date">'. str_todate(get_field( 'date_start' ));
    if (get_field( 'date_finish' ))
    echo ' - '. str_todate(get_field( 'date_finish' )) ;
      echo '</p>';  
    }
    echo '</div></div></div></a>';
    $i=$i+1;
}
}
}
wp_reset_postdata();
?>
</div>

</section>
<?php //do_action( 'avada_after_content' ); ?>
<?php get_footer();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
