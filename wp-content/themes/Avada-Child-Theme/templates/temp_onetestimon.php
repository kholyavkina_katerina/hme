<?php
/**
 * Template used for single posts and other post-types
 * that don't have a specific template.
 *
 * @package Avada
 * @subpackage Templates
 */

// Do not allow directly accessing this file.
if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Direct script access denied.' );
}
?>
<?php get_header(); ?>

<section id="content" style="width:100%;" <?php Avada()->layout->add_style( 'content_style' ); ?>>


	<?php while ( have_posts() ) : the_post(); ?>
		<article id="post-<?php the_ID(); ?>" class="testimon">
	
     <?php
     echo '<div class="col-md-4 testimonials">';
      echo '<div class="testimon_img">'.get_the_post_thumbnail( $post->ID, 'medium').'</div>';
      echo '</div><div class="col-md-8 testimonials descr">';
      
      if (get_field( 'tour_name' )) 
      echo '<div class="testimon_tour_name">' . get_field( 'tour_name' ) . '</div>';
      the_content();
      if (get_field( 'tour_name' )) 
      echo '<div class="testimon_name">';
      the_title( );
      echo '</div>';
      echo '<div class="facebook">' . get_field( 'facebook' ) . '</div>';
      if (get_field( 'screen' )){
      echo do_shortcode('[fusion_modal name="scr'.$post->ID.'" title="" size="large" background="" border_color="" show_footer="no" class="" id=""]<img src="'.get_field( 'screen' ).'">[/fusion_modal]');
      echo '<a href="#" data-toggle="modal" data-target=".fusion-modal.scr'.$post->ID.'">Оригинал отзыва</a></div>';
      }
      ?>

		</article>
	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
</section>
<?php //do_action( 'avada_after_content' ); ?>
<?php get_footer();

/* Omit closing PHP tag to avoid "Headers already sent" issues. */
