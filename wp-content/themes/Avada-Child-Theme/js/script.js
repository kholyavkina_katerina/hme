
(function ($) {

  "use strict";

  $(document).ready(function() {

    // Функция маски телефона

    jQuery(function($) {
      $('.phone_mask').mask('+38(099) 999-99-99');
    });


    // Функция передачи названия тура в форму заказа

    var tur_name = jQuery("h1").text();
    jQuery('.form_tour_name').val(tur_name);
    jQuery('#modal_info .modal-title').text(tur_name);


    // Слайдер иконок на странице тура

    var mySwiper = new Swiper ('#tour_icons .swiper-container', {
      slidesPerView: 6,
      loop: false,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      centerInsufficientSlides: true,
      breakpoints: {
        400: {
          slidesPerView: 2,
        },
        560: {
          slidesPerView: 3,
        },
        780: {
          slidesPerView: 4,
        },
        1200: {
          slidesPerView: 5,
        }
      }
    });


    // Слайдер с видео-отзывами
    var youtubeSwiper = new Swiper ('#youtube_carousel .swiper-container', {
      loop: true,
      slidesPerView: 2,
      spaceBetween: 50,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true
      },
      breakpoints: {
        780: {
          slidesPerView: 1,
        },
      }
    });


  // Создание календаря для блока "Экспедиция состоится также"
  (function () {
    var currentYear = (new Date()).getFullYear();
    var currentMonth = (new Date()).getMonth();
    var $calendar = $('.similar-tours-calendar');

    for (var i = 0; i < 2; i++) {
      var year = currentYear + i;
      $calendar.append('<div class="calendar__year calendar__year--' + year + '">' + year + '</div>');
      $calendar.append('<div class="calendar__months calendar__months--' + year + '"></div>');
    }

    $('.calendar__months').each(function() {
      for (var i = 0; i < 12; i++) {
        var className = $(this).hasClass('calendar__months--' + currentYear) && (i < currentMonth) ? 'disabled' : '';
        var monthName = new Date(2000, i, 1).toLocaleString('ru-ru', { month: 'long' }).substring(0, 3);
        $(this).append('<div class="calendar__month ' + className + '">' + monthName +'</div>');
      }
    });



    $('.similar-tours-item').each(function() {
      var href = $(this).data('href');
      var start = {
          year: $(this).data('start-year'),
          month: $(this).data('start-month') - 1,
      }
      if (start.year >= currentYear && start.month >= currentMonth) {
        $('.calendar__months--' + start.year + ' .calendar__month').eq(start.month).
            addClass("active").append('<a class="calendar__link" target="_blank" href="' + href + '"></a>');
      }
    });
  })();


  });

// Функция плавного скролинга

 $(".block_menu").on("click","a", function (event) {
//отменяем стандартную обработку нажатия по ссылке
        event.preventDefault();
//забираем идентификатор бока с атрибута href
        var id  = $(this).attr('href'),
//узнаем высоту от начала страницы до блока на который ссылается якорь
        top = $(id).offset().top;
//анимируем переход на расстояние - top за 1500 мс
        $('body,html').animate({scrollTop: top-60}, 1500);
});


   // фильтр категорий на странице Календарь туров

   // добавляем каждому туру класс .category_ekspedicia (если он не детский и не экстрим)
   $('.tour_item').each(function() {
     if (!$(this).hasClass('category_ekstrim-tur')) {
        $(this).addClass('category_ekspedicia');
     }
   })

   $()

   // при клике на кнопку фильтруем туры
   $('.calendar_filter').on("click", function (e) {
     e.preventDefault();
     $(this).addClass('active').siblings().removeClass('active');


     var category  = $(this).data('category');
     $('.tour_item').removeClass('hidden');
     console.log('.tour_item:not(' + category + ')');
     $('.tour_item:not(.' + category + ')').addClass('hidden');

     var rows = $('.list_tours');
     rows.each(function() {
       $(this).removeClass('hidden');
       var hiddenToursCount = $(this).find('.tour_item.hidden').length;
       var toursCount = $(this).find('.tour_item').length;
       if (hiddenToursCount === toursCount) {
         $(this).addClass('hidden');
       }
     });
   })



}(jQuery));

// Функция Ajax загрузки туров

jQuery('.row.country li').click(function(){
		var cat_id = jQuery(this).attr("class");

        cat_id=cat_id.replace('cat','');
       	var data = {
        'action': 'myaction',
        'query': 1,
        'cat_id' : cat_id
        };
    jQuery('.calendar').html('<span class="loading">Загрузка...</span>');
    jQuery.ajax({
    type: "POST",
    url: ajaxurl,
    data: data,
    success: function(data) {
      jQuery('.calendar').html(data);

    }

});
});

// Функция фиксирования меню туров при скролинге

function scrolltracknav(){

    if( jQuery( document ).scrollTop() > 650 ) {
            jQuery( '.block_menu' ).addClass('fixed');

        } else {
            jQuery( '.block_menu' ).removeClass('fixed');
        }

    }
    jQuery(window).scroll(scrolltracknav);

/*/ Функция отображения alt на самом фото*/
 jQuery('#a4 img').each(function () {
  jQuery(this).after(jQuery('<div class="carousel-title">').html(jQuery(this).attr('alt')));
})
