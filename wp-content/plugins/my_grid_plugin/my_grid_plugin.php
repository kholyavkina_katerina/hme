<?php
/*
Plugin Name: MY Grid
Plugin URI: http://websalon.com.ua
Description: Post Grid
Version: 1.0.0
Author: Vladimir
Author URI: http://websalon.com.ua
License: GPL2
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

function my_grid_plugin() {
?>	
<?php  
$today=date('Ymd');

$out='<div class="grid_block">';
$args = array('post_type' => 'tour',
    'posts_per_page' => 7,
    'meta_key' => 'date_start',
    'orderby' => 'meta_value_num',
    'order' => 'ASC',
    'meta_query' => array(
        array(
            'key' => 'date_start',
            'value' => $today,
            'compare' => '>',
        ),
    ),
);

$my_query = new WP_Query( $args );

if ( $my_query->have_posts() ) {
    $i=1;
    while ( $my_query->have_posts() ) {
        $my_query->the_post();

if ($i==1){        
$out .='<div class="col-md-6"><a href="'. get_permalink() .' "><div class="block first" style="background-image: url('. get_the_post_thumbnail_url( $id, 'medium') . ');">';
}
if ($i==2){        
$out .='<div class="col-md-6"><a href="'. get_permalink() .' "><div class="block second" style="background-image: url('. get_the_post_thumbnail_url( $id, 'medium') . ');">';
}
if ($i==3){        
$out .='<a href="'. get_permalink() .' "><div class="block third" style="background-image: url('. get_the_post_thumbnail_url( $id, 'medium') . ');">';
}
if ($i==4){        
$out .='<div class="col-md-6 second_line1"><a href="'. get_permalink() .' "><div class="col-md-6"><div class="block" style="background-image: url('. get_the_post_thumbnail_url( $id, 'medium') . ');">';
}
if ($i==6){        
$out .='<div class="col-md-6 second_line2"><a href="'. get_permalink() .' "><div class="col-md-6"><div class="block" style="background-image: url('. get_the_post_thumbnail_url( $id, 'medium') . ');">';
}
if (($i==5) || ($i==7)){        
$out .='<div class="col-md-6"><a href="'. get_permalink() .' "><div class="block" style="background-image: url('. get_the_post_thumbnail_url( $id, 'medium') . ');">';
}

$out .='<div class="tur_info"><h3 class="tur_name">' . $my_query->posts[$i-1]->post_title .'</h3>';
if (get_field( 'date_start' )) {
$out .=	'<p class="tur_date">'. str_todate(get_field( 'date_start' ));
if (get_field( 'date_finish' ))
$out .=	' - '. str_todate(get_field( 'date_finish' ));
$out .= '</p>';	
}
$out .='</div></div></a>';
if (($i==1) || ($i==3) || ($i==4) || ($i==6) || (($my_query->post_count==2) && ($i==2)) || (($my_query->post_count==4) && ($i==4) ) ) {
$out .='</div>';	
} 

if (($i==5) || ($i==7) || (($my_query->post_count==6) && ($i==6))){
$out .='</div></div>';
}
$i=$i+1;
 }
wp_reset_postdata();
$out .='</div>';
return $out;
}	
?>	
</div>		
<?php	}
	
add_shortcode('my_grid', 'my_grid_plugin');


?>
