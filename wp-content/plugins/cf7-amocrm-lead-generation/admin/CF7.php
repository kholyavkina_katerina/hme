<?php
namespace Cf7\AmoCRM\Lead\Generation\Admin;

use Cf7\AmoCRM\Lead\Generation\Includes\Bootstrap;
use Cf7\AmoCRM\Lead\Generation\Includes\CRM;
use Cf7\AmoCRM\Lead\Generation\Includes\CrmFields;

class CF7 extends \WPCF7_Service
{
    private static $instance = false;

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    protected function __construct()
    {
        add_action('wpcf7_init', [$this, 'registerService']);

        if ($this->is_active()) {
            add_filter('wpcf7_editor_panels', [$this, 'settingsPanels']);
            add_action('save_post_' . \WPCF7_ContactForm::post_type, [$this, 'saveSettings']);
        }
    }

    public function registerService()
    {
        $integration = \WPCF7_Integration::get_instance();
        $categories = ['crm' => $this->get_title()];

        foreach ($categories as $name => $category) {
            $integration->add_category($name, $category);
        }

        $services = ['cf7-amocrm-integration' => self::getInstance()];

        foreach ($services as $name => $service) {
            $integration->add_service($name, $service);
        }
    }

    // @codingStandardsIgnoreStart
    public function is_active()
    {
        // @codingStandardsIgnoreEnd
        $settings = \WPCF7::get_option(Bootstrap::OPTIONS_KEY);

        return !empty($settings['domain'])
        && !empty($settings['login'])
        && !empty($settings['hash']);
    }

    // @codingStandardsIgnoreStart
    public function get_title()
    {
        // @codingStandardsIgnoreEnd
        return esc_html__('Integration with amoCRM', 'cf7-amocrm-integration');
    }

    // @codingStandardsIgnoreStart
    public function get_categories()
    {
        // @codingStandardsIgnoreEnd
        return ['crm'];
    }

    public function icon()
    {
    }

    public function link()
    {
        echo '<a href="https://codecanyon.net/user/iwanttobelive">iwanttobelive</a>';
    }

    public function load($action = '')
    {
        if ('setup' == $action) {
            if (isset($_SERVER['REQUEST_METHOD']) && 'POST' == $_SERVER['REQUEST_METHOD']) {
                check_admin_referer('wpcf7-amocrm-integration-setup');
                $domain = isset($_POST['domain']) ? wp_unslash($_POST['domain']) : '';
                $login = isset($_POST['login']) ? wp_unslash($_POST['login']) : '';
                $hash = isset($_POST['hash']) ? wp_unslash($_POST['hash']) : '';
                $trackingId = isset($_POST['trackingId']) ? wp_unslash($_POST['trackingId']) : '';

                if (empty($domain) || empty($login) || empty($hash)) {
                    $redirect = $this->menuPageUrl(['message' => 'invalid', 'action' => 'setup']);
                } else {
                    $redirect = $this->menuPageUrl(['message' => 'success']);
                }

                \WPCF7::update_option(
                    Bootstrap::OPTIONS_KEY,
                    [
                        'domain' => $domain,
                        'login' => $login,
                        'hash' => $hash,
                        'trackingId' => $trackingId
                    ]
                );

                // Check connection and update CRM information of lead statuses and custom fields
                CRM::checkConnection();
                CRM::updateInformation();

                wp_safe_redirect($redirect);
                exit();
            }
        }
    }

    // @codingStandardsIgnoreStart
    public function admin_notice($message = '')
    {
        // @codingStandardsIgnoreEnd
        if ('invalid' === $message) {
            echo sprintf(
                '<div class="error notice notice-error is-dismissible"><p><strong>%1$s</strong>: %2$s</p></div>',
                esc_html__('ERROR', 'cf7-amocrm-integration'),
                esc_html__('To integrate with amoCRM, you must fill in all fields.', 'cf7-amocrm-integration')
            );
        } elseif ('success' === $message) {
            echo sprintf(
                '<div class="updated notice notice-success is-dismissible"><p>%s</p></div>',
                esc_html__('Settings successfully updated.', 'cf7-amocrm-integration')
            );
        }
    }

    public function display($action = '')
    {
        $settings = \WPCF7::get_option(Bootstrap::OPTIONS_KEY);
        ?>
        <p>
            <?php
            esc_html_e(
                'Formation of leads in amoCRM from the hits that users leave on your site, using '
                    . 'the Contact Form 7 plugin.',
                'cf7-amocrm-integration'
            );
            ?>
        </p>
        <?php
        if ('setup' == $action) {
            ?>
            <form method="post" action="<?php echo esc_url($this->menuPageUrl('action=setup')); ?>">
                <?php wp_nonce_field('wpcf7-amocrm-integration-setup'); ?>
                <table class="form-table">
                    <tbody>
                        <tr>
                            <th scope="row">
                                <label for="domain">
                                    <?php esc_html_e('Domain Name', 'cf7-amocrm-integration'); ?>
                                </label>
                            </th>
                            <td>
                                <input type="text"
                                    aria-required="true"
                                    value="<?php
                                    echo isset($settings['domain'])
                                        ? esc_attr($settings['domain'])
                                        : '';
                                    ?>"
                                    id="domain"
                                    placeholder="example.amocrm.ru"
                                    name="domain"
                                    class="regular-text">
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <label for="login">
                                    <?php esc_html_e('Login', 'cf7-amocrm-integration'); ?>
                                </label>
                            </th>
                            <td>
                                <input type="text"
                                    aria-required="true"
                                    value="<?php
                                    echo isset($settings['login'])
                                        ? esc_attr($settings['login'])
                                        : '';
                                    ?>"
                                    id="login"
                                    name="login"
                                    class="regular-text">
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <label for="hash">
                                    <?php esc_html_e('API key', 'cf7-amocrm-integration'); ?>
                                </label>
                            </th>
                            <td>
                                <input type="text"
                                    aria-required="true"
                                    value="<?php
                                    echo isset($settings['hash'])
                                        ? esc_attr($settings['hash'])
                                        : '';
                                    ?>"
                                    id="hash"
                                    name="hash"
                                    class="regular-text">
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">
                                <label for="trackingId">
                                    <?php esc_html_e('Google Analytics Tracking ID', 'cf7-amocrm-integration'); ?>
                                </label>
                            </th>
                            <td>
                                <input type="text"
                                    aria-required="true"
                                    value="<?php
                                    echo isset($settings['trackingId'])
                                        ? esc_attr($settings['trackingId'])
                                        : '';
                                    ?>"
                                    id="trackingId"
                                    name="trackingId"
                                    class="regular-text">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <?php
                echo sprintf(
                    '%1$s <a href="%2$s" target="_blank">%3$s</a>. %4$s.',
                    esc_html__('Plugin documentation: ', 'cf7-amocrm-integration'),
                    esc_url(CF7_AMOCRM_PLUGIN_URL . 'documentation/index.html#step-1'),
                    esc_html__('open', 'cf7-amocrm-integration'),
                    esc_html__('Or open the folder `documentation` in the plugin and open index.html', 'cf7-amocrm-integration')
                );
                ?>
                <p class="submit">
                    <input type="submit"
                        class="button button-primary"
                        value="<?php esc_attr_e('Save settings', 'cf7-amocrm-integration'); ?>"
                        name="submit">
                </p>
            </form>
            <?php
        } else {
            if ($this->is_active()) {
                ?>
                <table class="form-table">
                    <tbody>
                        <tr>
                            <th scope="row"><?php esc_html_e('Domain Name', 'cf7-amocrm-integration'); ?></th>
                            <td class="code"><?php echo esc_html($settings['domain']); ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?php esc_html_e('Login', 'cf7-amocrm-integration'); ?></th>
                            <td class="code"><?php echo esc_html($settings['login']); ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?php esc_html_e('API key', 'cf7-amocrm-integration'); ?></th>
                            <td class="code"><?php echo esc_html($settings['hash']); ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?php esc_html_e('Google Analytics Tracking ID', 'cf7-amocrm-integration'); ?></th>
                            <td class="code"><?php echo esc_html($settings['trackingId']); ?></td>
                        </tr>
                    </tbody>
                </table>
                <p>
                    <a href="<?php echo esc_url($this->menuPageUrl('action=setup')); ?>"
                        class="button">
                        <?php esc_html_e('Change settings', 'cf7-amocrm-integration'); ?>
                    </a>
                </p>
                <?php
            } else {
                ?>
                <p>
                    <?php
                    esc_html_e(
                        'To work with the plugin, you must configure integration with amoCRM.',
                        'cf7-amocrm-integration'
                    );
                    ?>
                </p>
                <p>
                    <a href="<?php echo esc_url($this->menuPageUrl('action=setup')); ?>"
                        class="button">
                        <?php esc_html_e('Go to setup', 'cf7-amocrm-integration'); ?>
                    </a>
                </p>
                <p class="description">
                    <?php
                    esc_html_e(
                        'The fields sent to the CRM are configured on the form editing page, on the "amoCRM" tab.',
                        'cf7-amocrm-integration'
                    );
                    ?>
                </p>
                <?php
            }
        }
    }

    public function settingsPanels($panels)
    {
        $panels['amocrm-panel'] = [
            'title' => esc_html__('amoCRM', 'cf7-amocrm-integration'),
            'callback' => [$this, 'panel']
        ];

        return $panels;
    }

    public function panel(\WPCF7_ContactForm $post)
    {
        $additionalFields = get_option(Bootstrap::OPTIONS_CUSTOM_FIELDS);
        $crmFields = new CrmFields();
        ?>
        <input type="hidden" name="cf7amoCRM[ENABLED]" value="0">
        <input
            type="checkbox"
            name="cf7amoCRM[ENABLED]" value="1"
            <?php checked(get_post_meta($post->id(), Bootstrap::META_PREFIX . 'ENABLED', true), true); ?>
            title="<?php
            esc_attr_e('Send the lead to amoCRM', 'cf7-amocrm-integration');
            ?>">
        <strong>
            <?php
            esc_html_e(
                'Send the lead to amoCRM',
                'cf7-amocrm-integration'
            );
            ?>
        </strong>
        <br><br>
        <?php
        echo esc_html(__(
            'In the following fields, you can use these mail-tags:',
            'contact-form-7'
        ));
        ?>
        <br>
        <?php
        $post->suggest_mail_tags();
        $currentType = get_post_meta($post->id(), Bootstrap::META_PREFIX . 'TYPE', true);
        $currentType = in_array($currentType, Bootstrap::$sendTypes) ? $currentType : 'leads';
        ?>
        <br><br>
        Utm-fields:<br>
        <span class="mailtag code">[utm_source]</span>
        <span class="mailtag code">[utm_medium]</span>
        <span class="mailtag code">[utm_campaign]</span>
        <span class="mailtag code">[utm_term]</span>
        <span class="mailtag code">[utm_content]</span>
        <br><br>
        <strong>
            <?php
            esc_html_e(
                'Choose the type of lead that will be generated in CRM:',
                'cf7-amocrm-integration'
            );
            ?>
        </strong>
        <br>
        <input type="radio"
            value="leads"
            name="cf7amoCRM[TYPE]"
            title="<?php esc_attr_e('Lead', 'cf7-amocrm-integration'); ?>"
            <?php checked($currentType, 'leads'); ?>
            > <?php esc_html_e('Deal (use fields contact and lead)', 'cf7-amocrm-integration'); ?>
        <br>
        <input type="radio"
            value="contacts"
            name="cf7amoCRM[TYPE]"
            title="<?php esc_html_e('Contact', 'cf7-amocrm-integration'); ?>"
            <?php checked($currentType, 'contacts'); ?>> <?php esc_html_e('Contact', 'cf7-amocrm-integration'); ?>
        <br>
        <input type="radio"
            value="unsorted"
            name="cf7amoCRM[TYPE]"
            title="<?php esc_html_e('Incoming Leads', 'cf7-amocrm-integration'); ?>"
            <?php checked($currentType, 'unsorted'); ?>
            > <?php esc_html_e('Incoming Leads (use fields contact and lead)', 'cf7-amocrm-integration'); ?>
        <br><br>
        <strong><?php esc_html_e('Fields for the "Lead"', 'cf7-amocrm-integration'); ?></strong>
        <br>
        <table class="form-table">
            <tbody>
                <?php
                foreach ($crmFields->leads as $key => $field) {
                    ?>
                    <tr>
                        <th scope="row">
                            <label for="__<?php
                            echo $key;
                            // escape ok
                            ?>">
                                <?php
                                echo esc_html($field['name']);
                                ?>
                                <?php
                                if (isset($field['required']) && $field['required'] === true) {
                                    echo '<span style="color:red;"> * </span>';
                                }
                                ?>
                            </label>
                        </th>
                        <td>
                            <?php
                            if ($key === 'status_id') {
                                $currentStatus = get_post_meta(
                                    $post->id(),
                                    Bootstrap::META_PREFIX . '-leads-' . $key,
                                    true
                                );
                                ?>
                                <select
                                    id="__<?php
                                    echo $key;
                                    // escape ok
                                    ?>"
                                    title="<?php
                                    echo $field['name'];
                                    // escape ok
                                    ?>"
                                    name="cf7amoCRM[leads][<?php
                                    echo $key;
                                    // escape ok
                                    ?>]">
                                    <?php
                                    $pipelines = get_option(Bootstrap::OPTIONS_PIPELINES);

                                    // Old format
                                    if (empty($pipelines)) {
                                        foreach (get_option(Bootstrap::OPTIONS_LEADS_STATUSES) as $status) {
                                            ?>
                                            <option value="<?php echo (int) $status['id']; ?>"
                                                <?php selected($currentStatus, (int) $status['id']); ?>>
                                                <?php echo esc_attr($status['name']); ?>
                                            </option>
                                            <?php
                                        }
                                    // New format
                                    } else {
                                        if ($currentStatus) {
                                            $explodeCurrentStatus = explode('.', $currentStatus);

                                            if (count($explodeCurrentStatus) < 2) {
                                                $currentStatus = current($pipelines)['id'] . '.' . $currentStatus;
                                            }
                                        }

                                        foreach ($pipelines as $pipelineID => $pipeline) {
                                            if (empty($pipeline['statuses'])) {
                                                continue;
                                            }

                                            echo '<optgroup label="' . esc_attr($pipeline['label']) . '">';

                                            foreach ($pipeline['statuses'] as $statusID => $status) {
                                                $statusValue = $pipelineID . '.' . $statusID;
                                                ?>
                                                <option value="<?php echo esc_attr($statusValue); ?>"
                                                    <?php selected($currentStatus, $statusValue); ?>>
                                                    <?php echo esc_attr($status['name']); ?>
                                                </option>
                                                <?php
                                            }

                                            echo '</optgroup>';
                                        }
                                    }
                                    ?>
                                </select>
                                <?php
                            } else {
                                ?>
                                <input id="__<?php
                                echo $key;
                                // escape ok
                                ?>"
                                type="text"
                                class="large-text code"
                                title="<?php
                                echo $field['name'];
                                // escape ok
                                ?>"
                                name="cf7amoCRM[leads][<?php
                                echo $key;
                                // escape ok
                                ?>]" value="<?php
                                echo esc_attr(get_post_meta(
                                    $post->id(),
                                    Bootstrap::META_PREFIX . '-leads-' . $key,
                                    true
                                ));
                                ?>">
                                <?php
                            }

                            if (isset($field['description'])) {
                                ?>
                                <p class="description"><?php echo esc_html($field['description']); ?></p>
                                <?php
                            }

                            if (isset($field['defaultValues'])) {
                                foreach ($field['defaultValues'] as $fieldKey => $fieldValue) {
                                    ?>
                                    <p><?php
                                        echo $fieldKey . ' - ' . $fieldValue;
                                        // escape ok
                                        ?></p>
                                    <?php
                                }
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            echo $field['type'];
                            // escape ok
                            ?>
                        </td>
                    </tr>
                    <?php
                }

                if (!empty($additionalFields['leads']) && is_array($additionalFields['leads'])) {
                    foreach ($additionalFields['leads'] as $field) {
                        // Not show plugin created analytics fields
                        if (isset($field['name']) && in_array($field['name'], CRM::$analyticsFields)) {
                            continue;
                        }

                        // Not show plugin created old analytics fields
                        if (isset($field['name']) && in_array($field['name'], CRM::$amoAnalyticsOldFields)) {
                            continue;
                        }

                        // Not show amoCRM created analytics fields
                        if (isset($field['code']) && in_array($field['code'], CRM::$amoAnalyticsFields)) {
                            continue;
                        }
                        ?>
                        <tr>
                            <th scope="row">
                                <label for="__<?php
                                echo $field['id'];
                                // escape ok
                                ?>">
                                    <?php
                                    echo $field['name'];
                                    // escape ok
                                    ?>
                                    <?php
                                    if (isset($field['required']) && $field['required'] === true) {
                                        echo '<span style="color:red;"> * </span>';
                                    }
                                    ?>
                                </label>
                            </th>
                            <td>
                                <input id="__<?php
                                echo $field['id'];
                                // escape ok
                                ?>"
                                    type="text"
                                    class="large-text code"
                                    title="<?php
                                    echo $field['name'];
                                    // escape ok
                                    ?>"
                                    name="cf7amoCRM[leads][<?php
                                    echo $field['id'];
                                    // escape ok
                                    ?>]" value="<?php
                                echo esc_attr(get_post_meta(
                                    $post->id(),
                                    Bootstrap::META_PREFIX . '-leads-' . $field['id'],
                                    true
                                ));
                                ?>">
                            </td>
                            <td>
                                <?php
                                echo $field['code'];
                                // escape ok
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
        <br><br>
        <strong><?php esc_html_e('Fields for "Contact"', 'cf7-amocrm-integration'); ?></strong>
        <br>
        <table class="form-table">
            <tbody>
                <?php
                foreach ($crmFields->contacts as $key => $field) {
                    ?>
                    <tr>
                        <th scope="row">
                            <label for="__<?php
                            echo $key;
                            // escape ok
                            ?>">
                                <?php
                                echo $field['name'];
                                // escape ok
                                ?>
                                <?php
                                if (isset($field['required']) && $field['required'] === true) {
                                    echo '<span style="color:red;"> * </span>';
                                }
                                ?>
                            </label>
                        </th>
                        <td>
                            <input id="__<?php
                            echo $key;
                            // escape ok
                            ?>"
                                type="text"
                                class="large-text code"
                                title="<?php
                                echo $field['name'];
                                // escape ok
                                ?>"
                                name="cf7amoCRM[contacts][<?php
                                echo $key;
                                // escape ok
                                ?>]" value="<?php
                            echo esc_attr(get_post_meta(
                                $post->id(),
                                Bootstrap::META_PREFIX . '-contacts-' . $key,
                                true
                            ));
                            ?>">
                            <?php
                            if (isset($field['description'])) {
                                ?>
                                <p class="description"><?php echo esc_html($field['description']); ?></p>
                                <?php
                            }

                            if (isset($field['defaultValues'])) {
                                foreach ($field['defaultValues'] as $fieldKey => $fieldValue) {
                                    ?>
                                    <p><?php
                                        echo $fieldKey . ' - ' . $fieldValue;
                                        // escape ok
                                        ?></p>
                                    <?php
                                }
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            echo $field['type'];
                            // escape ok
                            ?>
                        </td>
                    </tr>
                    <?php
                }

                if (!empty($additionalFields['contacts']) && is_array($additionalFields['contacts'])) {
                    foreach ($additionalFields['contacts'] as $field) {
                        ?>
                        <tr>
                            <th scope="row">
                                <label for="__<?php
                                echo $field['id'];
                                // escape ok
                                ?>">
                                    <?php
                                    echo $field['name'];
                                    // escape ok
                                    ?>
                                    <?php
                                    if (isset($field['required']) && $field['required'] === true) {
                                        echo '<span style="color:red;"> * </span>';
                                    }
                                    ?>
                                </label>
                            </th>
                            <td>
                                <input id="__<?php
                                echo $field['id'];
                                // escape ok
                                ?>"
                                    type="text"
                                    class="large-text code"
                                    title="<?php
                                    echo $field['name'];
                                    // escape ok
                                    ?>"
                                    name="cf7amoCRM[contacts][<?php
                                    echo $field['id'];
                                    // escape ok
                                    ?>]" value="<?php
                                echo esc_attr(get_post_meta(
                                    $post->id(),
                                    Bootstrap::META_PREFIX . '-contacts-' . $field['id'],
                                    true
                                ));
                                ?>">
                            </td>
                            <td>
                                <?php
                                echo $field['code'];
                                // escape ok
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
        <strong><?php esc_html_e('Fields for a "Note" to a "Contact"', 'cf7-amocrm-integration'); ?></strong>
        <br>
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row">
                        <label for="__note_contact">
                            <?php
                            echo esc_html_e('Note text', 'cf7-amocrm-integration');
                            ?>
                        </label>
                    </th>
                    <td>
                        <?php $note = get_post_meta($post->id(), Bootstrap::META_PREFIX . '-note', true); ?>
                        <textarea
                            id="__note_contact"
                            class="large-text code"
                            name="cf7amoCRM[note]"><?php echo esc_attr($note); ?></textarea>
                        <p class="description">
                            <?php
                            esc_html_e(
                                'ip, user agent, date and time, referrer - added auto',
                                'cf7-amocrm-integration');
                            ?>
                        </p>
                    </td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <?php
    }

    public function saveSettings($postID)
    {
        if (isset($_POST['cf7amoCRM']['ENABLED'])) {
            update_post_meta($postID, Bootstrap::META_PREFIX . 'ENABLED', wp_unslash($_POST['cf7amoCRM']['ENABLED']));
        }

        if (isset($_POST['cf7amoCRM']['TYPE'])) {
            update_post_meta($postID, Bootstrap::META_PREFIX . 'TYPE', wp_unslash($_POST['cf7amoCRM']['TYPE']));
        }

        $crmFields = new CrmFields();

        foreach ($crmFields->leads as $key => $_) {
            if (isset($_POST['cf7amoCRM']['leads'][$key])) {
                update_post_meta(
                    $postID,
                    Bootstrap::META_PREFIX . '-leads-' . $key,
                    wp_unslash($_POST['cf7amoCRM']['leads'][$key])
                );
            }
        }

        foreach ($crmFields->contacts as $key => $_) {
            if (isset($_POST['cf7amoCRM']['contacts'][$key])) {
                update_post_meta(
                    $postID,
                    Bootstrap::META_PREFIX . '-contacts-' . $key,
                    wp_unslash($_POST['cf7amoCRM']['contacts'][$key])
                );
            }
        }

        $additionalFields = get_option(Bootstrap::OPTIONS_CUSTOM_FIELDS);

        if (!empty($additionalFields['leads']) && is_array($additionalFields['leads'])) {
            foreach ($additionalFields['leads'] as $field) {
                if (isset($_POST['cf7amoCRM']['leads'][$field['id']])) {
                    update_post_meta(
                        $postID,
                        Bootstrap::META_PREFIX . '-leads-' . $field['id'],
                        wp_unslash($_POST['cf7amoCRM']['leads'][$field['id']])
                    );
                }
            }
        }

        if (!empty($additionalFields['contacts']) && is_array($additionalFields['contacts'])) {
            foreach ($additionalFields['contacts'] as $field) {
                if (isset($_POST['cf7amoCRM']['contacts'][$field['id']])) {
                    update_post_meta(
                        $postID,
                        Bootstrap::META_PREFIX . '-contacts-' . $field['id'],
                        wp_unslash($_POST['cf7amoCRM']['contacts'][$field['id']])
                    );
                }
            }
        }

        if (isset($_POST['cf7amoCRM']['note'])) {
            update_post_meta(
                $postID,
                Bootstrap::META_PREFIX . '-note',
                wp_unslash($_POST['cf7amoCRM']['note'])
            );
        }
    }

    protected function __clone()
    {
    }

    private function menuPageUrl($args = '')
    {
        $args = wp_parse_args($args, []);
        $url = menu_page_url('wpcf7-integration', false);
        $url = add_query_arg(['service' => 'cf7-amocrm-integration'], $url);

        if (!empty($args)) {
            $url = add_query_arg($args, $url);
        }

        return $url;
    }
}
