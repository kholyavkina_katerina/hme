=== Contact Form 7 - amoCRM - Integration ===
Contributors: https://codecanyon.net/user/iwanttobelive
Tags: amocrm, amocrm leads, business leads, contact form 7, contact form 7 amocrm, form, integration, lead finder, lead management, lead scraper, leads, marketing leads, sales leads.

== Description ==

The main task of this plugin is a send your Contact Form 7 forms directly to your amoCRM account.

= Features =

* Integrate your `Contact Form 7` forms with amoCRM;
* You can choice that your want to generate - lead, incoming lead or contact;
* You can set up each form personally, specify which information you want to get;
* Creation of the lead, occurs together with the creation / binding (used existing if there is) of the contact. (if their fields are filled);
* Supports uploading files (links to them are automatically added to the `note`);
* Supports for `utm` params in `URL` to use in custom fields;
* Multiple pipeline support;
* Image previews;
* Integrate unlimited Contact Form 7 forms;
* Sends Google Analytics data with lead to CRM;
* Super easy to set-up;

== Installation ==

1. Extract `cf7-amocrm-lead-generation.zip` and upload it to your `WordPress` plugin directory
(usually /wp-content/plugins ), or upload the zip file directly from the WordPress plugins page.
Once completed, visit your plugins page.
2. Be sure `Contact Form 7` Plugin is enabled.
3. Activate the plugin through the `Plugins` menu in WordPress.
4. Go to the `Contact Form 7` -> `Integration`.
5. Find `Integration with amoCRM` and click the button `Go to setup`.
6. Enter the domain name of your account `amoCRM` (without schema, i.e. http:// or https://).
7. Enter the login of your account `amoCRM`. Your should have permissions to adding contacts and deals in `amoCRM`.
8. Enter the API key. Your can find it on the settings page `amoCRM` account (your_amoCRM_domain/settings/profile/).
9. (ONLY FOR DEALS) If your use Google Analytics for monitoring your statistics, your can get more data.
Enter the Google Analytics Tracking ID.
Add send data in AmoCRM. (your_amoCRM_domain/settings/pipeline/leads/)
10. Save settings.
11. When editing forms your can see the tab `amoCRM`.

== Changelog ==

= 1.7.0 =
Feature: Search for an existing (by phone and email) contact, before creating a new one (by deal type).
Feature: Support for `utm` params in `URL` use custom fields.

= 1.6.1 =
Fixed: The name of the `unsorted` does not contain the name of the form.

= 1.6.0 =
Feature: Support for many pipeline.

= 1.5.0 =
Feature: Support for uploaded files. Links to them are automatically added to the `note`.
Fixed: Check whether plugin `Contact Form 7` is active on the `Network`.

= 1.4.0 =
Changed: send for a `note` to a `contact - now not only `incoming leads`
Added: creating a deal with a contact

= 1.3.0 =
Added: send `note` (ip, user agent, date and time, referrer ...) for `contact` (only incoming leads)

= 1.2.2 =
Fixed: save form settings

= 1.2.1 =
Fixed: duplicate analytics fields in CRM

= 1.2.0 =
Added: Ability to send `incoming lead`

= 1.1.1 =
Fixed: Google Analytics data - utm_source param

= 1.1.0 =
Added: Sends Google Analytics data with lead to CRM

= 1.0.0 =
Initial public release
