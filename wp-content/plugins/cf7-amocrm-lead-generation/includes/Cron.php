<?php
namespace Cf7\AmoCRM\Lead\Generation\Includes;

class Cron
{
    private static $instance = false;

    protected function __construct()
    {
        add_action('init', [$this, 'createCron']);
        add_action(Bootstrap::CRON_TASK, [$this, 'cronAction']);
        add_filter('cron_schedules', [$this, 'minutelyCron']);
    }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function createCron()
    {
        if (!wp_next_scheduled(Bootstrap::CRON_TASK)) {
            wp_schedule_event(time(), 'minutely', Bootstrap::CRON_TASK);
        }
    }

    public function cronAction()
    {
        CRM::updateInformation();
    }

    public function minutelyCron($schedules)
    {
        $schedules['minutely'] = ['interval' => 60];

        return $schedules;
    }

    private function __clone()
    {
    }
}
