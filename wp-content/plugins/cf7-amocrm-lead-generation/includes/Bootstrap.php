<?php
namespace Cf7\AmoCRM\Lead\Generation\Includes;

class Bootstrap
{
    const OPTIONS_KEY = 'cf7-amocrm-lead-generation-settings';
    const META_PREFIX = '_cf7-amocrm-lead-generation-';
    const CRON_TASK = 'cf7-amocrm-lead-generation-cron-task';
    const OPTIONS_LEADS_STATUSES = 'cf7-amocrm-lead-statuses';
    const OPTIONS_PIPELINES = 'cf7-amocrm-pipelines';
    const OPTIONS_CUSTOM_FIELDS = 'cf7-amocrm-custom-fields';
    const GOOGLE_ANALITICS_COOKIES = 'cf7-amocrm-ga-cookie';

    public static $plugin = '';

    public static $sendTypes = [
        'leads',
        'contacts',
        'unsorted'
    ];

    private static $instance = false;

    protected function __construct($file)
    {
        self::$plugin = $file;

        register_activation_hook(
            self::$plugin,
            ['Cf7\AmoCRM\Lead\Generation\Includes\Bootstrap', 'pluginActivation']
        );
        register_deactivation_hook(
            self::$plugin,
            ['Cf7\AmoCRM\Lead\Generation\Includes\Bootstrap', 'pluginDeactivation']
        );

        register_uninstall_hook(
            self::$plugin,
            ['Cf7\AmoCRM\Lead\Generation\Includes\Bootstrap', 'pluginUninstall']
        );

        add_action('init', [$this, 'googleAnaliticsCookies']);
    }

    public static function getInstance($file)
    {
        if (!self::$instance) {
            self::$instance = new self($file);
        }

        return self::$instance;
    }

    public function googleAnaliticsCookies()
    {
        if (isset($_GET['utm_source'])) {
            setcookie(
                self::GOOGLE_ANALITICS_COOKIES,
                wp_json_encode([
                    'utm_source' => isset($_GET['utm_source']) ? wp_unslash($_GET['utm_source']) : '',
                    'utm_medium' => isset($_GET['utm_medium']) ? wp_unslash($_GET['utm_medium']) : '',
                    'utm_campaign' => isset($_GET['utm_campaign']) ? wp_unslash($_GET['utm_campaign']) : '',
                    'utm_content' => isset($_GET['utm_content']) ? wp_unslash($_GET['utm_content']) : '',
                    'utm_term' => isset($_GET['utm_term']) ? wp_unslash($_GET['utm_term']) : ''
                ]),
                time() + 86400,
                '/'
            );
        }
    }

    public static function pluginActivation()
    {
        if (!is_plugin_active('contact-form-7/wp-contact-form-7.php')) {
            wp_die(
                esc_html__(
                    'To run the plug-in, you must first install and activate the Contact Form 7 plugin.',
                    'cf7-amocrm-integration'
                ),
                esc_html__(
                    'Error while activating the Contact Form 7 plugin - amoCRM - Lead Generation',
                    'cf7-amocrm-integration'
                ),
                [
                    'back_link' => true
                ]
            );
            // Escape ok
        }

        $roles = new \WP_Roles();
        add_option(self::OPTIONS_LEADS_STATUSES, [], '', 'no');
        add_option(self::OPTIONS_CUSTOM_FIELDS, [], '', 'no');

        foreach (self::capabilities() as $capGroup) {
            foreach ($capGroup as $cap) {
                $roles->add_cap('administrator', $cap);

                if (is_multisite()) {
                    $roles->add_cap('super_admin', $cap);
                }
            }
        }
    }

    public static function pluginDeactivation()
    {
        wp_clear_scheduled_hook(self::CRON_TASK);
    }


    public static function pluginUninstall()
    {
        delete_option(self::OPTIONS_LEADS_STATUSES);
        delete_option(self::OPTIONS_CUSTOM_FIELDS);
    }

    public static function capabilities()
    {
        $capabilities = [];
        $capabilities['core'] = ['manage_' . self::OPTIONS_KEY];
        flush_rewrite_rules(true);

        return $capabilities;
    }

    private function __clone()
    {
    }
}
