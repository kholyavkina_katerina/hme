<?php
namespace Cf7\AmoCRM\Lead\Generation\Includes;

class CF7
{
    private static $instance = false;

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    protected function __construct()
    {
        add_action('wpcf7_before_send_mail', [$this, 'onFormSubmit'], 10, 1);
    }

    public function onFormSubmit(\WPCF7_ContactForm $contactForm)
    {
        if (!class_exists('\\WPCF7_Submission')) {
            return;
        }

        $submission = \WPCF7_Submission::get_instance();

        if (!$submission || !($postedData = $submission->get_posted_data())) {
            return;
        }

        if (!get_post_meta($contactForm->id(), Bootstrap::META_PREFIX . 'ENABLED', true)) {
            return;
        }

        $utmFields = $this->parseUtmCookie();

        $postedData['utm_source'] = isset($utmFields['utm_source'])
            ? rawurldecode(wp_unslash($utmFields['utm_source']))
            : '';
        $postedData['utm_medium'] = isset($utmFields['utm_medium'])
            ? rawurldecode(wp_unslash($utmFields['utm_medium']))
            : '';
        $postedData['utm_campaign'] = isset($utmFields['utm_campaign'])
            ? rawurldecode(wp_unslash($utmFields['utm_campaign']))
            : '';
        $postedData['utm_term'] = isset($utmFields['utm_term'])
            ? rawurldecode(wp_unslash($utmFields['utm_term']))
            : '';
        $postedData['utm_content'] = isset($utmFields['utm_content'])
            ? rawurldecode(wp_unslash($utmFields['utm_content']))
            : '';

        $uploadedFiles = $submission->uploaded_files();

        $sendFields = [];
        $keys = array_map(function ($key) {
            return '[' . $key . ']';
        }, array_keys($postedData));
        $values = array_values($postedData);
        array_walk($values, function (&$value) {
            if (is_array($value)) {
                $value = implode(' ', $value);
            }
        });

        $type = get_post_meta($contactForm->id(), Bootstrap::META_PREFIX . 'TYPE', true);
        $type = in_array($type, Bootstrap::$sendTypes) ? $type : 'unsorted';

        $crmFields = new CrmFields();
        $additionalFields = get_option(Bootstrap::OPTIONS_CUSTOM_FIELDS);

        if ($type === 'contacts') {
            foreach ($crmFields->{$type} as $key => $_) {
                $value = get_post_meta($contactForm->id(), Bootstrap::META_PREFIX . '-' . $type . '-' . $key, true);

                if ($value) {
                    $sendFields[$type][$key] = trim(str_replace($keys, $values, $value));
                }
            }

            if (!empty($additionalFields[$type]) && is_array($additionalFields[$type])) {
                foreach ($additionalFields[$type] as $field) {
                    $value = get_post_meta(
                        $contactForm->id(),
                        Bootstrap::META_PREFIX . '-' . $type . '-' . $field['id'],
                        true
                    );

                    if ($value) {
                        $sendFields[$type]['custom_fields'][$field['id']] = trim(str_replace($keys, $values, $value));
                    }
                }
            }
        } else {
            foreach (['contacts', 'leads'] as $typeForeach) {
                foreach ($crmFields->$typeForeach as $key => $_) {
                    $value = get_post_meta(
                        $contactForm->id(),
                        Bootstrap::META_PREFIX . '-' . $typeForeach . '-' . $key,
                        true
                    );

                    if ($value) {
                        $sendFields[$typeForeach][$key] = trim(str_replace($keys, $values, $value));
                    }
                }

                if (!empty($additionalFields[$typeForeach]) && is_array($additionalFields[$typeForeach])) {
                    foreach ($additionalFields[$typeForeach] as $field) {
                        $value = get_post_meta(
                            $contactForm->id(),
                            Bootstrap::META_PREFIX . '-' . $typeForeach . '-' . $field['id'],
                            true
                        );

                        if ($value) {
                            $sendFields[$typeForeach]['custom_fields'][$field['id']]
                                = trim(str_replace($keys, $values, $value));
                        }
                    }
                }
            }
        }

        $note = trim(get_post_meta($contactForm->id(), Bootstrap::META_PREFIX . '-note', true));

        if ($note) {
            $sendFields['note'] = trim(str_replace($keys, $values, $note));
        }

        if (!empty($uploadedFiles)) {
            $sendFields['files'] = $this->prepareUploads($uploadedFiles);
        }

        CRM::send($sendFields, $type, $contactForm);
    }

    public function parseUtmCookie()
    {
        if (!empty($_COOKIE[Bootstrap::GOOGLE_ANALITICS_COOKIES])) {
            return json_decode(wp_unslash($_COOKIE[Bootstrap::GOOGLE_ANALITICS_COOKIES]), true);
        }

        return [];
    }

    public function prepareUploads($files)
    {
        $uploadsDir = wp_upload_dir();
        $uploadedFilesLinks = [];

        if (!file_exists($uploadsDir['basedir'] . '/cf7-amocrm-integration')) {
            mkdir($uploadsDir['basedir'] . '/cf7-amocrm-integration', 0777);
        }

        foreach ($files as $file) {
            if (!file_exists($file)) {
                continue;
            }

            $filePathInfo = pathinfo($file);
            $newFileName = uniqid()
                . '-'
                . $filePathInfo['basename'];

            $newFilePath = $uploadsDir['basedir']
                . '/cf7-amocrm-integration/'
                . $newFileName;

            copy($file, $newFilePath);

            $uploadedFilesLinks[] = $uploadsDir['baseurl']
                . '/cf7-amocrm-integration/'
                . $newFileName;
        }

        return $uploadedFilesLinks;
    }

    protected function __clone()
    {
        // Nothing
    }
}
