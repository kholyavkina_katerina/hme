<?php
namespace Cf7\AmoCRM\Lead\Generation\Includes;

class CrmFields
{
    public $leads;
    public $contacts;

    public function __construct()
    {
        $this->leads = [
            'name' => [
                'required' => true,
                'type' => esc_html__('String', 'cf7-amocrm-integration'),
                'name' => esc_html__('Lead name', 'cf7-amocrm-integration')
            ],
            // 'date_create'
            // 'last_modified'
            'status_id' => [
                'required' => false,
                'type' => esc_html__('Numeric', 'cf7-amocrm-integration'),
                'name' => esc_html__('Pipeline / Status', 'cf7-amocrm-integration')
            ],
            // 'pipeline_id'
            'price' => [
                'required' => false,
                'type' => esc_html__('Numeric', 'cf7-amocrm-integration'),
                'name' => esc_html__('Deal budget', 'cf7-amocrm-integration')
            ],
            'responsible_user_id' => [
                'required' => false,
                'type' => esc_html__('Numeric', 'cf7-amocrm-integration'),
                'name' => esc_html__('Responsible', 'cf7-amocrm-integration')
            ],
            // 'request_id'
            // 'linked_company_id'
            'tags' => [
                'required' => false,
                'type' => esc_html__('String', 'cf7-amocrm-integration'),
                'name' => esc_html__('Tags', 'cf7-amocrm-integration')
            ]
            // 'visitor_uid'
        ];

        $this->contacts = [
            'name' => [
                'required' => true,
                'type' => esc_html__('String', 'cf7-amocrm-integration'),
                'name' => esc_html__('Contact name', 'cf7-amocrm-integration')
            ],
            // 'request_id'
            // 'date_create'
            // 'last_modified'
            'responsible_user_id' => [
                'required' => false,
                'type' => esc_html__('Numeric', 'cf7-amocrm-integration'),
                'name' => esc_html__('Responsible', 'cf7-amocrm-integration')
            ],
            // 'linked_leads_id'
            'company_name' => [
                'required' => false,
                'type' => esc_html__('String', 'cf7-amocrm-integration'),
                'name' => esc_html__('Company name', 'cf7-amocrm-integration')
            ],
            // 'linked_company_id'
            'tags' => [
                'required' => false,
                'type' => esc_html__('String', 'cf7-amocrm-integration'),
                'name' => esc_html__('Tags', 'cf7-amocrm-integration')
            ]
        ];
    }

    private function __clone()
    {
    }
}
