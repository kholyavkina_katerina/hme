<?php
namespace Cf7\AmoCRM\Lead\Generation\Includes;

use AmoCRM\Client;
use AmoCRM\Exception;
use AmoCRM\Models\CustomField;
use AmoCRM\Models\Note;

class CRM
{
    public static $analyticsFields = [
        'GA UTM',
        'UTM CONTENT'
    ];

    public static $amoAnalyticsOldFields = [
        'UTM SOURCE',
        'UTM MEDIUM',
        'UTM CAMPAIGN',
        'UTM TERM'
    ];

    public static $amoAnalyticsFields = [
        'UTM_SOURCE',
        'UTM_MEDIUM',
        'UTM_CAMPAIGN',
        'UTM_TERM'
    ];

    public static function parseGoogleAnaliticsCookie()
    {
        if (!empty($_COOKIE[Bootstrap::GOOGLE_ANALITICS_COOKIES])) {
            return json_decode(wp_unslash($_COOKIE[Bootstrap::GOOGLE_ANALITICS_COOKIES]), true);
        }

        return [];
    }

    public static function addedGoogleAnaliticsInfoLead($lead)
    {
        $settings = \WPCF7::get_option(Bootstrap::OPTIONS_KEY);

        if (!empty($settings['trackingId']) && !empty($_COOKIE['_ga'])) {
            $additionalFields = get_option(Bootstrap::OPTIONS_CUSTOM_FIELDS);
            $cookieFields = self::parseGoogleAnaliticsCookie();

            if (!empty($additionalFields['leads']) && is_array($additionalFields['leads'])) {
                foreach ($additionalFields['leads'] as $field) {
                    if ($field['name'] == 'GA UTM') {
                        $clientId = explode('.', wp_unslash($_COOKIE['_ga']));

                        $lead->addCustomField(
                            $field['id'],
                            wp_json_encode(
                                [
                                    'ga' => [
                                        'trackingId' => $settings['trackingId'],
                                        'clientId' => $clientId[2] . '.' . $clientId[3]
                                    ],
                                    'utm' => [
                                        'source' => !empty($cookieFields['utm_source'])
                                            ? $cookieFields['utm_source']
                                            : '',
                                        'medium' => !empty($cookieFields['utm_medium'])
                                            ? $cookieFields['utm_medium']
                                            : '',
                                        'content' => !empty($cookieFields['utm_content'])
                                            ? $cookieFields['utm_content']
                                            : '',
                                        'campaign' => !empty($cookieFields['utm_campaign'])
                                            ? $cookieFields['utm_campaign']
                                            : '',
                                        'term' => !empty($cookieFields['utm_term'])
                                            ? $cookieFields['utm_term']
                                            : ''
                                    ],
                                    'data_source' => 'form'
                                ]
                            )
                        );
                    }

                    if ($field['name'] == 'UTM CONTENT') {
                        $lead->addCustomField(
                            $field['id'],
                            !empty($cookieFields['utm_content']) ? $cookieFields['utm_content'] : ''
                        );
                    }

                    if (!empty($field['code'])) {
                        switch ($field['code']) {
                            case 'UTM_SOURCE':
                                $lead->addCustomField(
                                    $field['id'],
                                    !empty($cookieFields['utm_source']) ? $cookieFields['utm_source'] : ''
                                );
                                break;
                            case 'UTM_MEDIUM':
                                $lead->addCustomField(
                                    $field['id'],
                                    !empty($cookieFields['utm_medium']) ? $cookieFields['utm_medium'] : ''
                                );
                                break;
                            case 'UTM_CAMPAIGN':
                                $lead->addCustomField(
                                    $field['id'],
                                    !empty($cookieFields['utm_campaign']) ? $cookieFields['utm_campaign'] : ''
                                );
                                break;
                            case 'UTM_TERM':
                                $lead->addCustomField(
                                    $field['id'],
                                    !empty($cookieFields['utm_term']) ? $cookieFields['utm_term'] : ''
                                );
                                break;
                            default:
                                // Nothing
                                break;
                        }
                    }
                }
            }
        }

        return $lead;
    }

    public static function send($fields, $type, $contactForm)
    {
        $settings = \WPCF7::get_option(Bootstrap::OPTIONS_KEY);
        $crmFields = new CrmFields();
        $additionalFields = get_option(Bootstrap::OPTIONS_CUSTOM_FIELDS);

        $prepareAdditionalContactFields = [];

        foreach ($additionalFields['contacts'] as $field) {
            $prepareAdditionalContactFields[$field['id']] = $field['code'];
        }

        try {
            $amo = new Client($settings['domain'], $settings['login'], $settings['hash']);

            if ($type == 'contacts') {
                $entity = $amo->contact;

                foreach ($fields[$type] as $key => $value) {
                    if (in_array($key, array_keys($crmFields->{$type}))) {
                        $entity[$key] = $value;
                    } elseif ($key === 'custom_fields' && !empty($value) && is_array($value)) {
                        foreach ($value as $id => $val) {
                            $entity->addCustomField($id, $val, 'WORK');
                        }
                    }
                }

                $entity->setNotes(self::generateContactNote($amo, $fields));

                $amo->contact->apiAdd([$entity]);
            } elseif ($type == 'leads') {
                $entity = $amo->lead;
                $entity = self::addedGoogleAnaliticsInfoLead($entity);

                // Set pipeline id
                if (isset($fields['leads']['status_id'])) {
                    $explodeCurrentStatus = explode('.', $fields['leads']['status_id']);

                    if (count($explodeCurrentStatus) > 1) {
                        $fields['leads']['pipeline_id'] = $explodeCurrentStatus[0];
                        $fields['leads']['status_id'] = $explodeCurrentStatus[1];
                    }
                }

                foreach ($fields['leads'] as $key => $value) {
                    if (in_array($key, array_keys($crmFields->{$type}))) {
                        $entity[$key] = $value;
                    } elseif ($key === 'custom_fields' && !empty($value) && is_array($value)) {
                        foreach ($value as $id => $val) {
                            $entity->addCustomField($id, $val);
                        }
                    }
                }

                $leadID = $amo->lead->apiAdd([$entity]);

                $entity = $amo->contact;

                $searchContactEmail = '';
                $searchContactPhone = '';

                foreach ($fields['contacts'] as $key => $value) {
                    if (in_array($key, array_keys($crmFields->{$type}))) {
                        $entity[$key] = $value;
                    } elseif ($key === 'custom_fields' && !empty($value) && is_array($value)) {
                        foreach ($value as $id => $val) {
                            if ($prepareAdditionalContactFields[$id] == 'EMAIL') {
                                $searchContactEmail = $val;
                            } elseif ($prepareAdditionalContactFields[$id] == 'PHONE') {
                                $searchContactPhone = $val;
                            }

                            $entity->addCustomField($id, $val, 'WORK');
                        }
                    }
                }

                $existsContact = false;

                if ($searchContactEmail) {
                    $existsContact = $entity->apiList(['query' => $searchContactEmail]);
                }

                if (!$existsContact && $searchContactPhone) {
                    $existsContact = $entity->apiList(['query' => $searchContactPhone]);
                }

                // Exists contact is found
                if ($existsContact) {
                    $existsContact = current($existsContact);
                    $leadIds = [$leadID];

                    if (!empty($existsContact['linked_leads_id'])) {
                        $leadIds = array_merge($existsContact['linked_leads_id'], $leadIds);
                    }

                    $entity = $amo->contact;
                    $entity->setLinkedLeadsId($leadIds);
                    $entity->apiUpdate($existsContact['id']);
                } else {
                    $entity->setNotes(self::generateContactNote($amo, $fields));
                    $entity->setLinkedLeadsId($leadID);

                    $amo->contact->apiAdd([$entity]);
                }
            } else {
                $unsorted = $amo->unsorted;
                $unsortedSourcedData = [];
                $lead = $amo->lead;
                $lead = self::addedGoogleAnaliticsInfoLead($lead);

                // Set pipeline id
                if (isset($fields['leads']['status_id'])) {
                    $explodeCurrentStatus = explode('.', $fields['leads']['status_id']);

                    if (count($explodeCurrentStatus) > 1) {
                        $unsorted['pipeline_id'] = $explodeCurrentStatus[0];
                    }
                }

                if (isset($fields['leads']['status_id'])) {
                    // Incoming leads not have status
                    unset($fields['leads']['status_id']);
                }

                foreach ($fields['leads'] as $key => $value) {
                    if (in_array($key, array_keys($crmFields->leads))) {
                        $lead[$key] = $value;
                        $unsortedSourcedData[$key . '_1'] = [
                            'type' => 'multitext',
                            'id' => $key,
                            'element_type' => '2',
                            'name' => $crmFields->leads[$key]['name'],
                            'value' => $value
                        ];
                    } elseif ($key === 'custom_fields' && !empty($value) && is_array($value)) {
                        foreach ($value as $id => $val) {
                            $lead->addCustomField($id, $val);

                            $name = '';

                            foreach ($additionalFields['leads'] as $field) {
                                if ($field['id'] == $id) {
                                    $name = $field['name'];
                                }
                            }

                            $unsortedSourcedData[$id . '_1'] = [
                                'type' => 'multitext',
                                'id' => $id,
                                'element_type' => '2',
                                'name' => $name,
                                'value' => $val
                            ];
                        }
                    }
                }

                $unsorted->addDataLead($lead);

                $contact = $amo->contact;

                foreach ($fields['contacts'] as $key => $value) {
                    if (in_array($key, array_keys($crmFields->contacts))) {
                        $contact[$key] = $value;
                        $unsortedSourcedData[$key . '_1'] = [
                            'type' => 'multitext',
                            'id' => $key,
                            'element_type' => '2',
                            'name' => $crmFields->contacts[$key]['name'],
                            'value' => $value
                        ];
                    } elseif ($key === 'custom_fields' && !empty($value) && is_array($value)) {
                        foreach ($value as $id => $val) {
                            $contact->addCustomField($id, $val, 'WORK');

                            $name = '';

                            foreach ($additionalFields['contacts'] as $field) {
                                if ($field['id'] == $id) {
                                    $name = $field['name'];
                                }
                            }

                            $unsortedSourcedData[$id . '_1'] = [
                                'type' => 'multitext',
                                'id' => $id,
                                'element_type' => '2',
                                'name' => $name,
                                'value' => $val
                            ];
                        }
                    }
                }

                $contact->setNotes(self::generateContactNote($amo, $fields));

                $unsorted->addDataContact($contact);

                $unsorted['source'] = \get_home_url();

                $unsorted['source_data'] = [
                    'data' => $unsortedSourcedData,
                    'form_id' => $contactForm->id(),
                    'form_type' => 1,
                    'origin' => [
                        'ip' => isset($_SERVER['REMOTE_ADDR']) ? wp_unslash($_SERVER['REMOTE_ADDR']) : '',
                        'referer' => isset($_SERVER['HTTP_REFERER']) ? wp_unslash($_SERVER['HTTP_REFERER']) : ''
                    ],
                    'date' => time(),
                    'from' => $contactForm->title() . ' - ' . \get_home_url(),
                    'form_name' => $contactForm->title()
                ];

                $unsorted->apiAddForms();
            }
        } catch (Exception $e) {
            if (defined('WP_DEBUG') && WP_DEBUG === true) {
                printf(
                    'Error (%d): %s' . "\n",
                    (int) $e->getCode(),
                    esc_html($e->getMessage())
                );
            }
        }
    }

    public static function updateInformation()
    {
        $settings = \WPCF7::get_option(Bootstrap::OPTIONS_KEY);

        $gaFieldsIsAdded = get_option('amocrm-cf7-ga-fields-is-added');

        try {
            $amo = new Client($settings['domain'], $settings['login'], $settings['hash']);

            if (!$gaFieldsIsAdded) {
                foreach (self::$analyticsFields as $analyticsField) {
                    $field = $amo->custom_field;
                    $field['name'] = $analyticsField;
                    $field['type'] = CustomField::TYPE_TEXT;
                    $field['element_type'] = CustomField::ENTITY_LEAD;
                    $field['origin'] = uniqid() . '_cf7';
                    $field->apiAdd();
                }

                update_option('amocrm-cf7-ga-fields-is-added', true);
            }

            $account = $amo->account->apiCurrent();

            update_option(Bootstrap::OPTIONS_CUSTOM_FIELDS, $account['custom_fields']);
            update_option(Bootstrap::OPTIONS_LEADS_STATUSES, $account['leads_statuses']);

            $pipelines = $amo->pipelines->apiList();
            update_option(Bootstrap::OPTIONS_PIPELINES, $pipelines);
        } catch (Exception $e) {
            if (defined('WP_DEBUG') && WP_DEBUG === true) {
                printf(
                    'Error (%d): %s' . "\n",
                    (int) $e->getCode(),
                    esc_html($e->getMessage())
                );
            }
        }
    }

    public static function checkConnection()
    {
        $settings = \WPCF7::get_option(Bootstrap::OPTIONS_KEY);

        try {
            $amo = new Client($settings['domain'], $settings['login'], $settings['hash']);
            $amo->account->apiCurrent();
        } catch (Exception $e) {
            // Clean failed information
            \WPCF7::update_option(
                Bootstrap::OPTIONS_KEY,
                []
            );

            wp_die(
                sprintf(
                    esc_html__(
                        'Response amoCRM: Error code (%d): %s. Check the settings.',
                        'cf7-amocrm-integration'
                    ),
                    (int) $e->getCode(),
                    esc_html($e->getMessage())
                ),
                esc_html__(
                    'An error occurred while verifying the connection to the amoCRM.',
                    'cf7-amocrm-integration'
                ),
                [
                    'back_link' => true
                ]
            );
            // Escape ok
        }
    }

    public static function generateContactNote($amo, $fields)
    {
        // Generate note from contact
        $note = $amo->note;

        $note['text'] = '';

        if (!empty($fields['files'])) {
            $note['text'] = esc_html__('Uploaded files:', 'cf7-amocrm-integration')
                . "\n";

            foreach ($fields['files'] as $link) {
                $note['text'] .= $link
                    . "\n";
            }
        }

        if (!empty($fields['note'])) {
            $note['text'] .= $fields['note']
                . "\n"
                . esc_html__('Additional information about the sender:', 'cf7-amocrm-integration')
                . "\n";
        } else {
            $note['text'] .= esc_html__('Additional information about the sender:', 'cf7-amocrm-integration')
                . "\n";
        }

        if (isset($_SERVER['REMOTE_ADDR'])) {
            $note['text'] .= esc_html__('IP-address: ', 'cf7-amocrm-integration')
                . wp_unslash($_SERVER['REMOTE_ADDR'])
                . "\n";
        }

        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $note['text'] .= 'User Agent: '
                . wp_unslash($_SERVER['HTTP_USER_AGENT'])
                . "\n";
        }

        $note['text'] .= esc_html__('Date and time: ', 'cf7-amocrm-integration')
            . date_i18n('Y-m-d H:i:s')
            . "\n";

        if (isset($_SERVER['HTTP_REFERER'])) {
            $note['text'] .= esc_html__('Referrer: ', 'cf7-amocrm-integration')
                . wp_unslash($_SERVER['HTTP_REFERER'])
                . "\n";
        }

        // Set note type - is required
        $note['note_type'] = Note::COMMON;

        return $note;
    }

    private function __construct()
    {
        // Nothing
    }

    private function __clone()
    {
        // Nothing
    }
}
