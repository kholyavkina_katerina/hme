<?php
/**
 * Plugin Name: Contact Form 7 - amoCRM - Integration
 * Description: Allows you to integrate your forms and amoCRM
 * Version: 1.7.0
 * Author: iwanttobelive
 * Author URI: https://codecanyon.net/user/iwanttobelive
 * Network: true
 * License: GPLv3
 * Text Domain: cf7-amocrm-integration
 * Domain Path: /languages/
 */

use Cf7\AmoCRM\Lead\Generation\Admin\CF7 as CF7Admin;
use Cf7\AmoCRM\Lead\Generation\Includes\Bootstrap;
use Cf7\AmoCRM\Lead\Generation\Includes\CF7 as CF7Includes;
use Cf7\AmoCRM\Lead\Generation\Includes\Cron;

if (!defined('ABSPATH')) {
    exit();
}

/*
 * Require for `is_plugin_active` function.
 */
require_once ABSPATH . 'wp-admin/includes/plugin.php';

load_theme_textdomain('cf7-amocrm-integration', __DIR__ . '/languages');

define('CF7_AMOCRM_PLUGIN_URL', plugin_dir_url(__FILE__));

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/includes/Bootstrap.php';
require __DIR__ . '/includes/CRM.php';
require __DIR__ . '/includes/CrmFields.php';
require __DIR__ . '/includes/CF7.php';

Bootstrap::getInstance(__FILE__);
CF7Includes::getInstance();

if (defined('DOING_CRON') && DOING_CRON) {
    include __DIR__ . '/includes/Cron.php';

    Cron::getInstance();
}

if (is_admin() && is_plugin_active('contact-form-7/wp-contact-form-7.php')) {
    add_action('plugins_loaded', function () {
        include __DIR__ . '/admin/CF7.php';

        CF7Admin::getInstance();
    });
}
