<?php
/*
Plugin Name: MY Tags
Plugin URI: http://websalon.com.ua
Description: Post Tags
Version: 1.0.0
Author: Vladimir
Author URI: http://websalon.com.ua
License: GPL2
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

function my_tags_plugin() {
?>	
<?php  
$cur_terms = get_the_terms( $post->ID, 'tour_tags');

			if ($cur_terms){
			$out='<p class="tags">';	
			foreach( $cur_terms as $cur_term ){
			$out .=	'<a href="'. get_term_link( (int)$cur_term->term_id, $cur_term->taxonomy ) .'">'. $cur_term->name .'</a>';
			}
			$out .= '</p>';
			}
			return $out;
}

add_shortcode('my_tags', 'my_tags_plugin');
?>