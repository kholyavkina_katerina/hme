<?php
/*
Plugin Name: Custom Sitemap Shortcode
Description: Plugin provides a sitemap shortcode. You can use shortcode on any page to display sitemap. You can fully customize your sitemap using plugin settings.
Plugin URI: http://www.divyanshiinfotech.com/custom-sitemap-shortcode/
Version: 2.5
Author: Anil Meena, Divyanshi Infotech
Author URI: http://www.anilmeena.com/
License: GNU General Public License v2.0 (or later)
License URI: http://www.opensource.org/licenses/gpl-license.php
Text Domain: custom-sitemap-shortcode
*/

require_once('lib/functions.php');

function wp_sitemap_add_styles() {
	wp_register_style( 'cst-main-style', plugin_dir_url( __FILE__ ) . 'css/sitemap.css' );
	wp_enqueue_style('cst-main-style');
}

function wp_sitemap_admin_add_styles(){
	wp_register_style( 'cst-admin-style', plugin_dir_url( __FILE__ ) . 'lib/css/admin.css' );
	wp_enqueue_style('cst-admin-style');
	wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'wp-color-picker' );
    wp_enqueue_script( 'underscore' );
}
add_action( 'wp_enqueue_scripts', 'wp_sitemap_add_styles' );

add_action( 'admin_enqueue_scripts', 'wp_sitemap_admin_add_styles' );

if( ! function_exists( 'cst_colorpicker_print_scripts' ) ) {
  function cst_colorpicker_print_scripts() {
  	// enqueue style
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_style( 'cs-wp-color-picker', plugins_url( 'css/cs-wp-color-picker.min.css', __FILE__ ), array( 'wp-color-picker' ), '1.0.0', 'all' );
    // enqueue scripts
    wp_enqueue_script( 'wp-color-picker' );
    wp_enqueue_script( 'cs-wp-color-picker', plugins_url( 'js/cs-wp-color-picker.min.js', __FILE__ ), array( 'wp-color-picker' ), '1.0.0', true );
  }
  add_action( 'admin_enqueue_scripts', 'cst_colorpicker_print_scripts' );
}
?>
