=== Custom Sitemap Shortcode ===
Contributors: anil.oxzin, divyanshiinfotech
Donate link: https://www.paypal.me/anilmeena/10
Tags: wordpress, sitemap, shortcode
Requires at least: 4.0
Tested up to: 4.9
Stable tag: 2.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Plugin provides a sitemap shortcode. You can use shortcode on any page to display sitemap. You can fully customize your sitemap using plugin settings.

== Description ==

Plugin provides a sitemap shortcode. You can use shortcode on any page to display sitemap. You can fully customize your sitemap using plugin settings.

== Installation ==
  
1. Upload \`custom-sitemap-template-plugin\` to the \`/wp-content/plugins/\` directory  
2. Activate the plugin through the 'Plugins' menu in WordPress  
3. Add [custom_sitamap] shortcode on any page where you want to display sitemap.


== Screenshots ==

1. Sitemap plugin settings.

2. Sitemap page shortcode view on page template using 3 column settings.

3. Sitemap page shortcode view on admin page.

4. Shortcode added on a sitemap page.

5. Custom CSS editor screenshot on admin page.

5. Color settings options on plugin settings.


== Frequently Asked Questions ==

= How can we display sitemap on page template =

You can put plugin shortcode [custom_sitamap] on any page from wordpress admin.

= Can I customize sitemap layout? =

Yes you can easily customize sitemap layout as required.

= Can I exclude any post/page from sitemap? =

Yes you can exclude any post/page/taxonomy by put its id in exclude named input field from plugin settings page.

= Is it possible to have two or more columns with the posts/categories in the sitemap?

Yes. You can manage 1, 2, 3, 4 or 5 columns from plugin settings to display posts/categories on sitemap page.

== Changelog ==

= 2.5 =
* Release Date: Nov 20, 2017
* Add color settings options on plugin settings.
* New screenshot added.
* Bug fixes.

= 2.4 =
* Release Date: Aug 30, 2016
* Add column options on plugin settings.
* New screenshots added.
* Add responsive css
* Bug fixes.

= 2.3.0 =
* Release Date: Jul 23, 2016
* Bug fixes.

= 2.2.0 =
* Release Date: Apr 30, 2016
* Add custom css editor option.
* Bug fixes.

= 2.1.0 =
* Release Date: Nov 20, 2015
* Bug fixes.

= 2.0.1 =
* Release Date: Oct 03, 2015
* Plugin information added.

= 2.0.0 =
* Release Date: Sep 30, 2015
* Update plugin settings using backend options.
* New Shortcode feature added.
* Bug fixes.

= 1.0.1 =
* Release Date: Jun 30, 2015
* Screenshots added.

= 1.0 =
* Release Date: Jun 18, 2015
* First version of plugin.

== Upgrade Notice ==

= 2.5 =
Add color settings options on plugin settings.
New screenshot added.
Bug fixes.

= 2.4 =
Add column options on plugin settings.
New screenshots added.
Add responsive css
Bug fixes.

= 2.3.0 =
Bug fixes.

= 2.2.0 =
Add custom css editor option.
Bug fixes.

= 2.1.0 =
Bug fixes.

= 2.0.1 =
Bug fixes.
Update plugin information.

= 2.0.0 =
This is stable version. 
Added new shortcode function.
Remove sitemap template feature.

= 1.0.1 =
Screenshots added.
Bug fixes.

= 1.0 =
This is first version of plugin.
