<?php
global $wpdb;
$cst_version = "2.5";
$cst_installed_ver = get_option( "cst_version" );
if ( $cst_installed_ver != $cst_version ) {
	update_option( "cst_version", $cst_version );
}

add_action( 'admin_menu', 'sitemap_shortcode_plugin_menu' );

function sitemap_shortcode_plugin_menu(){
	add_submenu_page( 'options-general.php', 'Sitemap Settings', 'Sitemap Settings', 'manage_options', 'sitemap_settings', 'sitemap_settings_callback'); 
	add_action('admin_init','register_sitemap_settings');
}

function register_sitemap_settings(){
	register_setting('sitemap_settings-group','sitemap_post_list');
	register_setting('sitemap_settings-group','sitemap_cat_list');
	register_setting('sitemap_settings-group','sitemap_hide_post');
	register_setting('sitemap_settings-group','sitemap_hide_cat');
	register_setting('sitemap_settings-group','cst_settings_arr');
}

function sitemap_settings_callback(){
	settings_fields('sitemap_settings-group');
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}?>
	<div class="wrap">
		<h2>Sitemap Settings</h2>
		<?php if(isset($_POST['set_sitemap'])){ ?>
			<div>
				<style>
					#option-saved-box{
						display: inline-block;
					}	
				</style>
				<script>
				jQuery(document).ready(function($){
				   	setTimeout(function(){
				  		$("#option-saved-box").fadeOut("slow", function () {
				  			$("#option-saved-box").remove();
				      		});
				 
					}, 3000);
				});
				</script>
				<p id="option-saved-box">
					<strong><?php _e( 'Options saved', 'customtheme' ); ?></strong>
				</p>
			</div>
			<?php $checkpost = $_POST['checkpost'];
			$checkcat = $_POST['checkcat'];
			$hidepost = sanitize_text_field( stripslashes( $_POST['hidepost']) );
			$hidecat = sanitize_text_field( stripslashes( $_POST['hidecat'] ) );

			$hidepost = preg_replace(
			  array(
			    '/[^\d,]/',    // Matches anything that's not a comma or number.
			    '/(?<=,),+/',  // Matches consecutive commas.
			    '/^,+/',       // Matches leading commas.
			    '/,+$/'        // Matches trailing commas.
			  ),
			  '',              // Remove all matched substrings.
			  $hidepost
			);

			$hidecat = preg_replace(
			  array(
			    '/[^\d,]/',    // Matches anything that's not a comma or number.
			    '/(?<=,),+/',  // Matches consecutive commas.
			    '/^,+/',       // Matches leading commas.
			    '/,+$/'        // Matches trailing commas.
			  ),
			  '',              // Remove all matched substrings.
			  $hidecat
			);

			$data = array(	
				'sitemap_post_list' => $checkpost,
				'sitemap_cat_list' => $checkcat,
				'sitemap_hide_post' => $hidepost,
				'sitemap_hide_cat' => $hidecat,
			);

			update_option('cst_settings_arr', serialize($data));

			update_option('sitemap_post_list', $checkpost);
			update_option('sitemap_cat_list', $checkcat);
			update_option('sitemap_hide_post', $hidepost);
			update_option('sitemap_hide_cat', $hidecat);
		} 

		if(isset($_POST['cst_save_custom_css'])){ 

			$msg = 0;

			$cst_admin_css_setting['cst_admin_css_field'] =   filter_var($_POST['cst_admin_css_field'], FILTER_SANITIZE_STRING);
			$cst_admin_css_setting['cst_admin_sitemap_bg_color'] =   filter_var($_POST['cst_admin_sitemap_bg_color'], FILTER_SANITIZE_STRING);
			$cst_admin_css_setting['cst_admin_post_title_closed_color'] =   filter_var($_POST['cst_admin_post_title_closed_color'], FILTER_SANITIZE_STRING);
			$cst_admin_css_setting['cst_admin_post_title_opened_color'] =   filter_var($_POST['cst_admin_post_title_opened_color'], FILTER_SANITIZE_STRING);
			$cst_admin_css_setting['cst_admin_sitemap_link_color'] =   filter_var($_POST['cst_admin_sitemap_link_color'], FILTER_SANITIZE_STRING);
			$cst_admin_css_setting['cst_admin_sitemap_link_hover_color'] =   filter_var($_POST['cst_admin_sitemap_link_hover_color'], FILTER_SANITIZE_STRING);

			$options = get_option('cst_admin_css_setting');
			if (empty($options)) {
				add_option('cst_admin_css_setting', $cst_admin_css_setting);
				$msg = 1;
			}
			else{
				update_option( 'cst_admin_css_setting', $cst_admin_css_setting );
				$msg = 1;
			}

		} 

		if(isset($_POST['cst_save_sitemap_column'])){ 

			$msg = 0;

			$cst_sitemap_column_setting['cst_sitemap_column_field'] =   filter_var($_POST['cst_sitemap_column_field'], FILTER_SANITIZE_STRING);

			$options = get_option('cst_sitemap_column_setting');
			if (empty($options)) {
				add_option('cst_sitemap_column_setting', $cst_sitemap_column_setting);
				$msg = 1;
			}
			else{
				update_option( 'cst_sitemap_column_setting', $cst_sitemap_column_setting );
				$msg = 1;
			}

		} 

		$options = get_option('cst_admin_css_setting');
		if(empty($options['cst_admin_sitemap_bg_color'])): 
			$options['cst_admin_sitemap_bg_color'] =  "#eeeeee";
		endif;
		if(empty($options['cst_admin_post_title_closed_color'])): 
			$options['cst_admin_post_title_closed_color'] =  "#000000";
		endif;
		if(empty($options['cst_admin_post_title_opened_color'])): 
			$options['cst_admin_post_title_opened_color'] =  "#FFA500";
		endif;
		if(empty($options['cst_admin_sitemap_link_color'])): 
			$options['cst_admin_sitemap_link_color'] =  "#000000";
		endif;
		if(empty($options['cst_admin_sitemap_link_hover_color'])): 
			$options['cst_admin_sitemap_link_hover_color'] =  "#FFA500";
		endif;

		update_option( 'cst_admin_css_setting', $options );
		?>

		<h3>How to use sitemap plugin?</h3>
		<p>Just add [custom_sitamap] shortcode on any page where you want to display sitemap.</p>
		<p>If you want to add sitemap shortcode on any page template then add <code>display_custom_sitamap_option();</code> in your template file.</p>

		<h3>Include Post Types</h3>
			<p>Please select which post type would you want to display on sitemap.</p>

			<?php  $args = array(
			   'public'   => true,
			   //'_builtin' => false
			);

			$output = 'names'; // names or objects, note names is the default
			$operator = 'and'; // 'and' or 'or'

			$post_types = get_post_types( $args, $output, $operator ); ?>

			<form action="" method="post" onsubmit="return numericValidation();">
			<?php foreach ( $post_types as $post_type ) : ?>
				<p>
				<?php if($post_type != "attachment"){
					$lists = get_option('sitemap_post_list', true);
					if(!is_array($lists)){
						$lists = array();
					}
					if(count($lists) > 0): 
						//echo "non zero";
					if (in_array($post_type, $lists)) { ?>
						<input value="<?php echo $post_type;?>" type="checkbox" name="checkpost[]" checked>
					<?php } else {  ?>
						<input value="<?php echo $post_type;?>" type="checkbox" name="checkpost[]">
					<?php }
					else :
						//echo "zero";?>
						<input value="<?php echo $post_type;?>" type="checkbox" name="checkpost[]">
					<?php endif;
					echo $post_type; 
				}?>
				</p>
			<?php endforeach; ?>
			<h3>Exclude Post/Page Id</h3>
			<p>Please enter post/page id which you want to hide on sitemap. You can enter multiple post ids using comma seperated.</p>
			<div id="hide_post_wrapper">
				<span><strong>Enter Post/Page Ids: </strong></span>
				<input type="text" id="hidepost" name="hidepost" value="<?php echo get_option('sitemap_hide_post', true);?>">
				<span class="cst-error" id="cst-error1">Error. Please enter number & comma (,) only.</span>
			</div>

			<h3>Include Taxonomies</h3>
			<p>Please select which taxonomy would you want to display on sitemap.</p>
				<?php $args = array("public" => true);
				$taxonomies = get_taxonomies($args); ?>
			
			<?php foreach ( $taxonomies as $taxonomy ) : ?>
				<p>
				<?php if($taxonomy != "post_format"){
					$filter_tags = strpos($taxonomy, "cat");
					if($filter_tags !== false){
						$catlist = get_option('sitemap_cat_list', true);
						if(!is_array($catlist)){
							$catlist = array();
						}
						if(count($catlist) > 0):
							//echo "non zero";
						if (in_array($taxonomy, $catlist)) { ?>
							<input value="<?php echo $taxonomy;?>" type="checkbox" name="checkcat[]" checked>
						<?php } else { ?>
							<input value="<?php echo $taxonomy;?>" type="checkbox" name="checkcat[]">
						<?php } 
						else : 
							//echo "zero"; ?>
							<input value="<?php echo $taxonomy;?>" type="checkbox" name="checkcat[]">
						<?php endif;
						echo $taxonomy;
					}
				}?>
				</p>
			<?php endforeach; ?>
			<h3>Exclude Term Id</h3>
			<p>Please enter term id which you want to hide on sitemap. You can enter multiple term ids using comma seperated.</p>
			<div id="hide_cat_wrapper">
				<span><strong>Enter Term Ids: </strong></span>
				<input type="text" id="hidecat" name="hidecat" value="<?php echo get_option('sitemap_hide_cat', true);?>">
				<span class="cst-error" id="cst-error2">Error. Please enter number & comma (,) only.</span><br />
				<input type="submit" name="set_sitemap" class="button button-primary button-large" value="Submit">
			</div>
			</form>

			<hr>

			<!--[Sitemap Column Form]-->
			<?php $options = get_option('cst_sitemap_column_setting'); ?>
			<form method="post" action="">
				<h3>Sitemap Column Options</h3>
				<p>You can manage your sitemap columns here that you want to display using shortcode.</p>
				<span><strong>Select Sitemap Column: </strong></span>
				<select name="cst_sitemap_column_field">
					<option value="1" <?php echo ($options['cst_sitemap_column_field'] == 1 ? 'selected' : '1');?> >1 Column (Default)</option>
					<option value="2" <?php echo ($options['cst_sitemap_column_field'] == 2 ? 'selected' : '2');?> >2 Columns</option>
					<option value="3" <?php echo ($options['cst_sitemap_column_field'] == 3 ? 'selected' : '3');?> >3 Columns</option>
					<option value="4" <?php echo ($options['cst_sitemap_column_field'] == 4 ? 'selected' : '4');?> >4 Columns</option>
					<option value="5" <?php echo ($options['cst_sitemap_column_field'] == 5 ? 'selected' : '5');?> >5 Columns</option>
				</select><br>
				<input type="submit" name="cst_save_sitemap_column" class="button button-primary button-large" value="Submit">
			</form>
			<!--[Sitemap Column Form]-->

			<hr>

			<!--[Custom CSS Form]-->
			<?php $options = get_option('cst_admin_css_setting'); ?>
			<form method="post" action="">
				<h3>Sitemap Color Settings</h3>
				<p>Sitemap Section Background: <input type="text" name="cst_admin_sitemap_bg_color" class="cs-wp-color-picker" data-default-color="#eeeeee" value="<?php echo esc_html(stripslashes($options['cst_admin_sitemap_bg_color'])); ?>"></p>
				<p>Post Type Title Closed: <input type="text" name="cst_admin_post_title_closed_color" class="cs-wp-color-picker" data-default-color="#000000" value="<?php echo esc_html(stripslashes($options['cst_admin_post_title_closed_color'])); ?>"></p>
				<p>Post Type Title Opened: <input type="text" name="cst_admin_post_title_opened_color" class="cs-wp-color-picker" data-default-color="#FFA500" value="<?php echo esc_html(stripslashes($options['cst_admin_post_title_opened_color'])); ?>"></p>
				<p>Sitemap Link Title: <input type="text" name="cst_admin_sitemap_link_color" class="cs-wp-color-picker" data-default-color="#000000" value="<?php echo esc_html(stripslashes($options['cst_admin_sitemap_link_color'])); ?>"></p>
				<p>Sitemap Link Title Hover: <input type="text" name="cst_admin_sitemap_link_hover_color" class="cs-wp-color-picker" data-default-color="#FFA500" value="<?php echo esc_html(stripslashes($options['cst_admin_sitemap_link_hover_color'])); ?>"></p>

				<h3>Custom CSS Editor</h3>
				<p>You can add your custom css here.</p>
				<textarea name="cst_admin_css_field" cols="50" rows="6" ><?php echo esc_html(stripslashes($options['cst_admin_css_field'])); ?></textarea><br />
				<input type="submit" name="cst_save_custom_css" class="button button-primary button-large" value="Submit">
			</form>
			<!--[Custom CSS Form]-->

		    <script type="text/javascript">
			function numericValidation() {
			    var numbers = /^([0-9 ,]+)?$/;
			    var txt = document.getElementById('hidepost');
			    var txtcat = document.getElementById('hidecat');

			    if (!(txt.value.match(numbers))) {
				document.getElementById("cst-error1").style.display = "inline-block";
				return false;
			    }
			    else {	
				if (!(txtcat.value.match(numbers))) {

					document.getElementById("cst-error2").style.display = "inline-block";
					return false;
				}
				else { 
				      	return true;
			    	}
			    }
		        }
			</script>
	</div>
<?php } 

add_shortcode('custom_sitamap', 'display_custom_sitamap_option');

function display_custom_sitamap_option(){ 

$options = get_option('cst_admin_css_setting');
if(!empty($options['cst_admin_css_field'])): ?>
	<style>
	<?php echo $options['cst_admin_css_field'];?>
	</style>
<?php endif;

if(empty($options['cst_admin_sitemap_bg_color'])): 
	$options['cst_admin_sitemap_bg_color'] =  "#eeeeee";
endif;
if(empty($options['cst_admin_post_title_closed_color'])): 
	$options['cst_admin_post_title_closed_color'] =  "#000000";
endif;
if(empty($options['cst_admin_post_title_opened_color'])): 
	$options['cst_admin_post_title_opened_color'] =  "#FFA500";
endif;
if(empty($options['cst_admin_sitemap_link_color'])): 
	$options['cst_admin_sitemap_link_color'] =  "#000000";
endif;
if(empty($options['cst_admin_sitemap_link_hover_color'])): 
	$options['cst_admin_sitemap_link_hover_color'] =  "#FFA500";
endif;

update_option( 'cst_admin_css_setting', $options );
?>
<style>
#toggle-view h2.cst-tab-title, #toggle-view span.cst-list-tab-icon{color: <?php echo $options['cst_admin_post_title_closed_color'];?>}
#toggle-view h2.cst-tab-title.openSitemapTab, #toggle-view span.cst-list-tab-icon.openSitemapTab{color: <?php echo $options['cst_admin_post_title_opened_color'];?>}
#toggle-view .panel .sitemap a.cst-item-links{color: <?php echo $options['cst_admin_sitemap_link_color'];?>}
#toggle-view .panel .sitemap a.cst-item-links:hover{color: <?php echo $options['cst_admin_sitemap_link_hover_color'];?>}
</style>
<?php

$col_options = get_option('cst_sitemap_column_setting');
if(!empty($col_options['cst_sitemap_column_field'])): 
	$cols = $col_options['cst_sitemap_column_field'];
else: 
	$cols = 1;
endif; ?>

<div class="sitemap-container">

<?php $cst_settings_arr = get_option('cst_settings_arr', null);
if ($cst_settings_arr !==  null) { 
	$cst_settings_val = unserialize($cst_settings_arr); ?>

	<!--[start sitemap-content]-->
	<div class="sitemap-content" id="cst-content-tab-wrapper">

		<ul id="toggle-view" class="cst-tab-list-wrapper">

		<?php $lists = $cst_settings_val['sitemap_post_list'];
		$postignore = $cst_settings_val['sitemap_hide_post'];

		if(!$lists){
			echo "<p>Nothing to display here.</p>";
		} else {
		
			$postex_arr = explode(",", $postignore);

			foreach($lists as $list) :
				global $wp_post_types;
				$obj = $wp_post_types[$list];
				$posttitle = $obj->labels->name; ?>
				<li class="cst-tab-title-wrapper">
					<h2 class="cst-tab-title"><?php echo $posttitle;?></h2>
						<span class="cst-list-tab-icon">+</span>
					<div class="panel cst-list-panel-wrapper">
						<ul class="cst-list-panel">
						<?php $args = array(
							'post_type'     => $list,
							'posts_per_page'   => -1,
							'orderby'	=> 'title',
							'order'		=> 'ASC',
							'post__not_in'  => $postex_arr
						);
						$query = new WP_Query( $args );

						// The Loop
						if ( $query->have_posts() ) :
							while ( $query->have_posts() ) :
								$query->the_post();?>
								<li class="sitemap sitemap_cols_<?php echo $cols;?>">
									<a class="cst-item-links" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</li>
							<?php endwhile;
						else :
							echo "No $posttitle Found";
						endif; wp_reset_postdata();?>
						</ul>
					</div>
				</li>
			<?php endforeach; 
		}

		$catlists = $cst_settings_val['sitemap_cat_list'];
		$catignore = $cst_settings_val['sitemap_hide_cat'];

		if(!$catlists){
			//echo "<p>Please select a taxonomy type to display here.</p>";
		}  else {
					
			$hidecat = explode(",", $catignore);
			foreach($catlists as $catlist) :
				$catresult = get_taxonomy( $catlist );
				
				if($catresult->label == "Categories"){
				$args = array(
					'orderby' => 'name',
					'hide_empty' => 0,
					'exclude' =>  $hidecat
				);
				$categories = get_categories( $args );?>
				<li class="cst-tab-title-wrapper">
					<h2 class="cst-tab-title"><?php echo $catresult->label;?></h2>
					<?php //var_dump($catresult);?>
						<span class="cst-list-tab-icon">+</span>
					<div class="panel cst-list-panel-wrapper">
						<ul class="cst-list-panel">

						<?php foreach ( $categories as $category ) {
							echo '<li class="sitemap sitemap_cols_'.$cols.'"><a class="cst-item-links" href="' . get_category_link( $category->term_id ) . '">' . $category->name . '</a></li>';
						}
						?>

						</ul>
					</div>
				</li>
				<?php } else {

					/*var_dump($catresult->rewrite['slug']); 
					var_dump($catresult->query_var);*/

					$filter_tags = strpos($catresult->query_var, "cat");

					if($filter_tags !== false){
						$args = array(
							'orderby' => 'name',
							'hide_empty' => 0,
							'exclude' =>  $hidecat,
							'taxonomy' => $catresult->query_var
						);
						
						$categories = get_terms( $args );?>
						<li class="cst-tab-title-wrapper">
							<h2 class="cst-tab-title"><?php echo $catresult->label;?></h2>
							<?php //var_dump($catresult);?>
								<span class="cst-list-tab-icon">+</span>
							<div class="panel cst-list-panel-wrapper">
								<ul class="cst-list-panel">

								<?php foreach ( $categories as $category ) {
									echo '<li class="sitemap sitemap_cols_'.$cols.'"><a class="cst-item-links" href="' . get_category_link( $category->term_id ) . '">' . $category->name . '</a></li>';
								}
								?>

								</ul>
							</div>
						</li>
					<?php }
				} 
			 endforeach;
		}?>

		</ul>

		<script type="text/javascript">
		jQuery(document).ready(function ($) {
	
			$('#toggle-view li').click(function () {

				var text = $(this).children('div.panel');

				if (text.is(':hidden')) {
					text.slideDown('200');
					/*$(this).children('span').css('color', '#FFA500');
					$(this).children('h2').css('color', '#FFA500');*/
					$(this).children('h2').addClass('openSitemapTab');
					$(this).children('span').addClass('openSitemapTab');
					$(this).children('span').html('-');		
				} else {
					text.slideUp('200');
					/*$(this).children('span').css('color', '#000');
					$(this).children('h2').css('color', '#000');*/
					$(this).children('h2').removeClass('openSitemapTab');
					$(this).children('span').removeClass('openSitemapTab');
					$(this).children('span').html('+');		
				}
		
			});

		});
		</script>
        </div>	
	<!--[end sitemap-content]-->

	<?php } ?>

</div>
<!--[end sitemap-container]-->

<?php }

